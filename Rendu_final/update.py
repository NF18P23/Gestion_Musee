from datetime import datetime
import json
import re
import psycopg2
from lecture import lecture_auteur, lecture_musee, lecture_exposition, lecture_exposition_permanente, lecture_exposition_temporaire, lecture_oeuvre, lecture_emprunte

# test
connexion = psycopg2.connect("dbname ='dbnf18p049' user='nf18p049' password='sbX7OFf3sl77' host='tuxa.sme.utc'")
cur = connexion.cursor()

# validate_date 1
def validate_date_1(date_text):
    try:
        if date_text != datetime.strptime(date_text, "%d-%m-%Y").strftime('%d-%m-%Y'):
            raise ValueError
        return True
    except ValueError:
        return False

# validate_date 2
def validate_date_2(date_text):
    try:
        if date_text != datetime.strptime(date_text, "%d/%m/%Y").strftime('%d/%m/%Y'):
            raise ValueError
        return True
    except ValueError:
        return False

def validate_time_format(time_str):
    pattern = r'^([0-1][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$'
    match = re.match(pattern, time_str)
    
    if match:
        hour = int(match.group(1))
        minute = int(match.group(2))
        second = int(match.group(3))
        
        if hour <= 23 and minute <= 59 and second <= 59:
            return True
    
    return False

def update_musee(cur,connexion):
    print("---------Mettre à jour d'un musée----------")
    # lecture
    lecture_musee(cur,connexion)
    test = False
    while not test:
        musee = input("Le nom du musée à modifier : ")
        sql = f"SELECT nom FROM Musee WHERE nom='{musee}';"
        cur.execute(sql)
        if cur.fetchone():
            test = True
        else:
            print(f"[WARN] Le musée d'idientifiant {musee} n'existe pas")
    rue = ""
    while len(rue) == 0:
        rue = input("Nouvelle Rue: ")
        if len(rue) == 0:
            print("[WARN] rue ne peut pas être vide!")
    postcode = ""
    while len(postcode) == 0:
        postcode = input("Nouveau Postcode: ")
        if len(postcode) == 0:
            print("[WARN] postcode ne peut pas être vide!")
    ville = ""
    while len(ville) == 0:
        ville = input("Nouvelle Ville: ")
        if len(ville) == 0:
            print("[WARN] ville ne peut pas être vide!")
    adresse = {
        "rue": rue,
        "postcode": postcode,
        "ville": ville,
    }
    adresse = json.dumps(adresse)
    try:
        sql = f"UPDATE Musee SET adresse='{adresse}' WHERE nom='{musee}';"
        cur.execute(sql)
        connexion.commit()
        print("[INFO] Le musee a été modifié avec succès.")
    except psycopg2.Error as e:
        print("[ERREUR] Une erreur s'est produite lors de mise à jour du musée :", e)
        return

def update_auteur(cur,connexion):
    print("---------Mettre à jour d'un auteur----------")
    lecture_auteur(cur,connexion)
    test = False
    while not test:
        try:
            id = int(input("L'identifiant de l'auteur à modifier : "))
            sql = f"SELECT * FROM Auteur WHERE id={id};"
            cur.execute(sql)
            if cur.fetchone():
                test = True
            else:
                print(f"[WARN] L'auteur {id} n'existe pas")
        except ValueError:
            print("[WARN] Veuillez entrer un entier")

    choix = input("1 ---- changer le nom de l'auteur\n2 ---- changer le prénom\n3 ---- changer la date de naissance\n4 ---- changer la date de mort\nvotre choix : ")
    if choix == "1":
        nom = ""
        while len(nom) == 0:
            nom = input("Le nouveau nom de l'auteur : ")
            if len(nom) == 0:
                print("[WARN] Nom ne peut pas être vide!")
        sql = f"UPDATE Auteur SET nom='{nom}' WHERE id={id};"
        try:
            cur.execute(sql)
            connexion.commit()
            print("[INFO] L'auteur a été modifié avec succès.")
        except psycopg2.DataError as e:
            print("[Erreur] Une erreur s'est produite lors de la modification de l'auteur :", e)
    elif choix == "2":
        prenom = input("Le nouveau prénom de l'auteur : ")
        sql = f"UPDATE Auteur SET prenom='{prenom}' WHERE id={id};"
        try:
            cur.execute(sql)
            connexion.commit()
            print("[INFO] L'auteur a été modifié avec succès.")
        except psycopg2.DataError as e:
            print("[Erreur] Une erreur s'est produite lors de la modification de l'auteur :", e)
    elif choix == "3":
        date_naissance = input("la nouvelle date de naissance de l'auteur (DD-MM-YYYY) : ")
        while (validate_date_1(date_naissance)==False) :
            print("[WARN] Le format de la date de naissance(DD-MM-YYYY)!")
            date_naissance = input("la nouvelle date de naissance de l'auteur (DD-MM-YYYY) : ")
        sql = f"UPDATE Auteur SET date_naissance=TO_DATE('{date_naissance}', 'DD/MM/YYYY') WHERE id={id};"
        try:
            cur.execute(sql)
            connexion.commit()
            print("[INFO] L'auteur a été modifié avec succès.")
        except psycopg2.DataError as e:
            print("[Erreur] Une erreur s'est produite lors de la modification de l'auteur :", e)
    elif choix == "4":
        date_mort = input("La nouvelle date de mort de l'auteur (DD-MM-YYYY) : ")
        while (date_mort != '' and validate_date_1(date_mort)==False) :
            print("[WARN] Le format de la date de mort (DD-MM-YYYY)!")
            date_mort = input("la nouvelle date de mort de l'auteur (DD-MM-YYYY) : ")
        if date_mort != '':
            sql = f"UPDATE Auteur SET date_mort=TO_DATE('{date_mort}', 'DD/MM/YYYY') WHERE id={id};"
        else:
            sql = f"UPDATE Auteur SET date_mort=NULL WHERE id={id};" 
        try:
            cur.execute(sql)
            connexion.commit()
            print("[INFO] L'auteur a été modifié avec succès.")
        except psycopg2.DataError as e:
            print("[Erreur] Une erreur s'est produite lors de la modification de l'auteur :", e)
    else:
        print("[WARN] Veuillez entrer un entier entre 1 et 4")

def update_emprunte(cur,connexion):
    print("---------Mettre à jour de l'emprunte----------")
    test = False
    while not test:
        musee = input("Le muséé de l'emprunte à modifier : ")
        oeuvre = input("L'oeuvre de l'emprunte à modifier : ")
        sql = f"SELECT * FROM Emprunte WHERE musee='{musee}' and oeuvre='{oeuvre}';"
        cur.execute(sql)
        if cur.fetchone():
            test = True
        else:
            print(f"[WARn] L'emprunt {musee}-{oeuvre} n'existe pas")
    choix = input("1 ---- changer le muséé de l'emprunte\n2 ---- changer l'oeuvre de l'emprunte\n3 ---- changer la date de début de l'emprunte\n4 ---- changer la date de fin de l'emprunte\nvotre choix : ")
    if choix == "1":
        new_musee = ""
        verif=0
        while(verif==0) :
            # afficher la liste de musée
            lecture_musee(cur,connexion)
            new_musee = input("Le nouveau musée de l'emprunte : ")
            sql = "SELECT * FROM Musee WHERE nom='%s'" % new_musee
            cur.execute(sql)
            row = cur.fetchone()
            if (row) :
                verif = 1
        sql = f"UPDATE Emprunte SET musee='{new_musee}' WHERE musee='{musee}' and oeuvre='{oeuvre}';"
        try:
            cur.execute(sql)
            connexion.commit()
            print("[INFO] L'emprunte a été modifié avec succès.")
        except psycopg2.DataError as e:
            print("[Erreur] Une erreur s'est produite lors de la modification de l'emprunte :", e)
    elif choix == "2":
        new_oeuvre = ""
        verif = 0
        # oeuvre
        while(verif==0) :
            # afficher la liste de exposition
            lecture_musee(cur, connexion)
            new_oeuvre = input("Le nouveau musée de l'emprunte : ")
            sql = "SELECT * FROM Oeuvre WHERE titre='%s'" % new_oeuvre
            cur.execute(sql)
            row = cur.fetchone()
            if (row) :
                verif = 1
        sql = f"UPDATE Emprunte SET oeuvre='{new_oeuvre}' WHERE musee='{musee}' and oeuvre='{oeuvre}';"
        try:
            cur.execute(sql)
            connexion.commit()
            print("[INFO] L'emprunte a été modifié avec succès.")
        except psycopg2.DataError as e:
            print("[Erreur] Une erreur s'est produite lors de la modification de l'emprunte :", e)
    elif choix == "3":
        date_debut = input("Nouvelle Date début de l'emprunte (DD/MM/YYYY): ")
        while (validate_date_2(date_debut) == False) :
            print("[WARN] Le format de date début ne suffit pas!")
            date_debut = input("Nouvelle Date début de l'emprunte (DD/MM/YYYY): ")
        sql = f"UPDATE Emprunte SET date_debut=TO_DATE('{date_debut}', 'DD/MM/YYYY') WHERE musee='{musee}' and oeuvre='{oeuvre}';"
        try:
            cur.execute(sql)
            connexion.commit()
            print("[INFO] L'emprunte a été modifié avec succès.")
        except psycopg2.DataError as e:
            print("[Erreur] Une erreur s'est produite lors de la modification de l'emprunte :", e)
    elif choix == "4":
        date_fin = input("Nouvelle Date fin de l'emprunte (DD/MM/YYYY): ")
        while (validate_date_2(date_fin) == False) :
            print("[WARN] Le format de date fin ne suffit pas!")
            date_fin = input("Nouvelle Date fin de l'emprunte (DD/MM/YYYY): ")
        sql = f"UPDATE Emprunte SET date_fin=TO_DATE('{date_fin}', 'DD/MM/YYYY') WHERE musee='{musee}' and oeuvre='{oeuvre}';"
        try:
            cur.execute(sql)
            connexion.commit()
            print("[INFO] L'emprunte a été modifié avec succès.")
        except psycopg2.DataError as e:
            print("[Erreur] Une erreur s'est produite lors de la modification de l'emprunte :", e)
    else:
        print("[WARN] Veuillez entrer un entier entre 1 et 4")


def update_oeuvre(cur,connexion):
    print("---------Mettre à jour de l'oeuvre----------")
    test = False
    while not test:
        titre = input("Le titre de l'oeuvre à modifier : ")
        sql = f"SELECT * FROM Oeuvre WHERE titre='{titre}';"
        cur.execute(sql)
        if cur.fetchone():
            test = True
        else:
            print(f"[WARN] L'oeuvre {titre} n'existe pas")
    choix = input("1 ---- changer le type de l'oeuvre\n2 ---- changer la date de l'oeuvre\n3 ---- changer la dimension de l'oeuvre\n4 ---- changer le prix d'acquisition\n5 ---- changer le restaurée de l'oeuvre\n6 ---- changer l'auteur de l'oeuvre\n7 ---- changer exposition de l'oeuvre\n8 ---- changer musée de l'oeuvre\nvotre choix : ")
    if choix == "1":
        typ = input("Nouveau type de l'oeuvre (peinture, sculpture, photo) : ")    # verification type de l'oeuvre
        # verification type de l'oeuvre
        while typ not in ['peinture','sculpture','photo']:
            print("[WARN] Le type de l'oeuvre n'est pas bon!")
            typ = input("Nouveau type de l'oeuvre (peinture, sculpture, photo) : ")
        sql = f"UPDATE Oeuvre SET type='{typ}' WHERE titre='{titre}';"
        try:
            cur.execute(sql)
            connexion.commit()
            print("[INFO] L'oeuvre a été modifiée avec succès.")
        except psycopg2.DataError as e:
            print("[Erreur] Une erreur s'est produite lors de la modification de l'oeuvre :", e)
    elif choix == "2":
        date = input("Nouvelle Date (DD/MM/YYYY): ")
        while (validate_date_2(date) == False) :
            print("[WARN] Le format de date ne suffit pas!")
            date = input("Nouvelle Date (DD/MM/YYYY): ")
        sql = f"UPDATE Oeuvre SET date=TO_DATE('{date}', 'DD/MM/YYYY') WHERE titre='{titre}';"
        try:
            cur.execute(sql)
            connexion.commit()
            print("[INFO] L'oeuvre a été modifiée avec succès.")
        except psycopg2.DataError as e:
            print("[Erreur] Une erreur s'est produite lors de la modification de l'oeuvre :", e)
    elif choix == "3":
        # dimension: length, width
        # length de l'oeuvre
        length = input("Length de l'oeuvre: ")
        # length > 0
        while not length or int(length) <= 0 :
            print("[WARN] Le length de l'oeuvre doit > 0 et Non Null!")
            length = input("Length de l'oeuvre: ")
        # width de l'oeuvre
        width = input("Width de l'oeuvre: ")
        dimension = {
            "length": length,
            "width": width,
        }
        dimension = json.dumps(dimension)
        sql = f"UPDATE Oeuvre SET dimension='{dimension}' WHERE titre='{titre}';"
        try:
            cur.execute(sql)
            connexion.commit()
            print("[INFO] L'oeuvre a été modifiée avec succès.")
        except psycopg2.DataError as e:
            print("[Erreur] Une erreur s'est produite lors de la modification de l'oeuvre :", e)
    elif choix == "4":
        # prix_acquisition
        prix_acquisition = input("Nouveau Prix d'acquisition de l'oeuvre: ")
        # verification de prix d'acquisition
        while prix_acquisition != '':
            try:
                prix_acquisition = int(prix_acquisition)
                if prix_acquisition > 0:
                    break
                else:
                    print("[WARN] Le prix d'acquisition de l'oeuvre doit >= 0!")
                    prix_acquisition = input("Nouveau Prix d'acquisition de l'oeuvre: ")
            except ValueError:
                print("[WARN] Entrez un nombre!")
                prix_acquisition = input("Nouveau Prix d'acquisition de l'oeuvre: ")
        sql = f"UPDATE Oeuvre SET prix_acquisition = '{prix_acquisition}' WHERE titre='{titre}';"
        try:
            cur.execute(sql)
            connexion.commit()
            print("[INFO] L'oeuvre a été modifiée avec succès.")
        except psycopg2.DataError as e:
            print("[Erreur] Une erreur s'est produite lors de la modification de l'oeuvre :", e)
    elif choix == "5":
        restauree = input("Restaurée? (y pour yes) : ")
        if restauree == "y":
            restauree = True
        else:
            restauree = False 
        sql = f"UPDATE Oeuvre SET restauree = '{restauree}' WHERE titre='{titre}';"
        try:
            cur.execute(sql)
            connexion.commit()
            print("[INFO] L'oeuvre a été modifiée avec succès.")
        except psycopg2.DataError as e:
            print("[Erreur] Une erreur s'est produite lors de la modification de l'oeuvre :", e)
    elif choix == "6":
        verif=0
        while(verif==0) :
            # affiche_auteur()
            auteur_nom = input("Le nom de l'auteur : ")
            sql = "SELECT * FROM Auteur WHERE nom='%s'" % auteur_nom
            cur.execute(sql)
            row = cur.fetchone()
            if (row) :
                verif = 1
                # obtenir auteur_id
                aut = row[0]
        sql = f"UPDATE Oeuvre SET auteur = '{aut}' WHERE titre='{titre}';"
        try:
            cur.execute(sql)
            connexion.commit()
            print("[INFO] L'oeuvre a été modifiée avec succès.")
        except psycopg2.DataError as e:
            print("[Erreur] Une erreur s'est produite lors de la modification de l'oeuvre :", e)
    elif choix == "7":
        verif = 0
        while(verif==0) :
            # afficher la liste d'exposition
            lecture_exposition(cur, connexion)
            exposition = input("Le nom d'exposition : ")
            sql = "SELECT * FROM Exposition WHERE nom='%s'" % exposition
            cur.execute(sql)
            row = cur.fetchone()
            if (row) :
                verif = 1
        sql = f"UPDATE Oeuvre SET exposition = '{exposition}' WHERE titre='{titre}';"
        try:
            cur.execute(sql)
            connexion.commit()
            print("[INFO] L'oeuvre a été modifiée avec succès.")
        except psycopg2.DataError as e:
            print("[Erreur] Une erreur s'est produite lors de la modification de l'oeuvre :", e)
    elif choix == "8":
        verif=0
        while(verif==0) :
            # afficher la liste de musée
            lecture_musee(cur, connexion)
            musee = input("Le nom de musée: ")
            sql = "SELECT * FROM Musee WHERE nom='%s'" % musee
            cur.execute(sql)
            row = cur.fetchone()
            if (row) :
                verif = 1   
        sql = f"UPDATE Oeuvre SET musee = '{musee}' WHERE titre='{titre}';"
        try:
            cur.execute(sql)
            connexion.commit()
            print("[INFO] L'oeuvre a été modifiée avec succès.")
        except psycopg2.DataError as e:
            print("[Erreur] Une erreur s'est produite lors de la modification de l'oeuvre :", e)
    else:
        print("[WARN] Veuillez entrer un entier entre 1 et 8")

def update_exposition_permanente(cur,connexion):
    print("---------Mettre à jour de l'oeuvre permanente----------")
    test = False
    while not test:
        nom = input("Le nom de l'exposition permanente à modifier : ")
        sql = f"SELECT nom FROM ExpositionPermanente WHERE nom='{nom}';"
        cur.execute(sql)
        if cur.fetchone():
            test = True
        else:
            print(f"[WARN] L'exposition permanente {nom} n'existe pas")
    choix = input("1 ---- ajouter un guide\n2 ---- supprimmer un guide\nvotre choix : ")
    if choix == "1":
        # 
        sql = f"SELECT ep.guide FROM expositionpermanente ep WHERE nom='{nom}';"
        cur.execute(sql)
        row = cur.fetchone()
        guides = row[0]
        # nouveau guide
        guide_nom = input("Nom de guide : ")
        while len(guide_nom) == 0:
            print("[WARN] Nom de guide Non NULL!")
            guide_nom = input("Nom de guide : ")
        
        guide_prenom = input("Prénom de guide : ")
        while len(guide_prenom) == 0:
            print("[WARN] Prénom de guide Non NULL!")
            guide_prenom = input("Prénom de guide : ")

        guide_adresse = input("Adresse de guide : ")
        while len(guide_adresse) == 0:
            print("[WARN] Adresse de guide Non NULL!")
            guide_adresse = input("Adresse de guide : ")

        guide_date_embauche = input("Date embauché de guide : ")
        while len(guide_date_embauche) == 0:
            print("[WARN] Date embauché de guide Non NULL!")
            guide_date_embauche = input("Date embauché de guide : ")

        # creneau
        nb_creneau = input("Nombre de créneau: ")
        while(not nb_creneau or int(nb_creneau) <= 0):
            print("[WARN] Nombre de créneau doit > 0")
            nb_creneau = input("Combien de créneau de guide : ")
        creneaux = []
        nb_creneau = int(nb_creneau)
        while nb_creneau > 0:
            jour = input("Jour (lundi-dimanche) : ")
            while not jour or jour not in ['lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi','dimanche']:
                print("[WARN] Jour Non NULL et doit etre 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi','dimanche'")
                jour = input("Jour (lundi-dimanche) : ")
            while 1:
                heure_debut = input("Heure début (HH:MM:SS) : ")
                if not validate_time_format(heure_debut):
                    print("[WARN] Le format de l'heure début n'est pas bon")
                    heure_debut = input("Heure début (HH:MM:SS) : ")
                heure_fin = input("Heure fin (HH:MM:SS) : ")
                if not validate_time_format(heure_debut):
                    print("[WARN] Le format de l'heure fin n'est pas bon")
                    heure_fin = input("Heure fin (HH:MM:SS) : ")
                # verifier le format de heure debut et fin, heure_fin > heure_debut
                # datetime
                heure_debut_dt = datetime.datetime.strptime(heure_debut, "%H:%M:%S")
                heure_fin_dt = datetime.datetime.strptime(heure_fin, "%H:%M:%S")
                if heure_debut_dt < heure_fin_dt:
                    break
                else:
                    print("[WARN] L'heure fin doit être supérieure à l'heure début")
                    heure_fin = input("Heure fin (HH:MM:SS) : ")
                    heure_fin_dt = datetime.datetime.strptime(heure_fin, "%H:%M:%S")
            creneau = {
                "jour": jour,
                "heure_debut": heure_debut,
                "heure_fin": heure_fin,
            }
            creneaux.append(creneau)
            nb_creneau -= 1
        guide = {
            "nom": guide_nom,
            "prenom": guide_prenom,
            "adresse": guide_adresse,
            "date_embauche": guide_date_embauche,
            "creneau": creneaux,
        }
        guides.append(guide)
        json_guides = json.dumps(guides)
        sql = f"UPDATE expositionpermanente SET guide = '{json_guides}' WHERE nom='{nom}';"
        try:
            cur.execute(sql)
            connexion.commit()
            print("[INFO] L'exposition permanente a été modifiée avec succès.")
        except psycopg2.DataError as e:
            print("[Erreur] Une erreur s'est produite lors de la modification de l'exposition permanenete :", e)
    elif choix == '2':
        # 
        sql = f"SELECT ep.guide FROM expositionpermanente ep WHERE nom='{nom}';"
        cur.execute(sql)
        row = cur.fetchone()
        guides = row[0]
        guide_nom = input("Nom de guide que vous voulez supprimer: ")
        while len(guide_nom) == 0:
            print("[WARN] Nom de guide Non NULL!")
            guide_nom = input("Nom de guide que vous voulez supprimer: ")
        
        guide_prenom = input("Prénom de guide que vous voulez supprimer: ")
        while len(guide_prenom) == 0:
            print("[WARN] Prénom de guide Non NULL!")
            guide_prenom = input("Prénom de guide que vous voulez supprimer: ")
        index_delete = -1
        index = 0
        for guide in guides:
            if guide["nom"] == guide_nom and guide["prenom"] == guide_prenom:
                index_delete = index
                break
            index += 1
        if index_delete != -1:
            del guides[index_delete]
        json_guides = json.dumps(guides)
        sql = f"UPDATE expositionpermanente SET guide = '{json_guides}' WHERE nom='{nom}';"
        try:
            cur.execute(sql)
            connexion.commit()
            print("[INFO] L'exposition permanente a été modifiée avec succès.")
        except psycopg2.DataError as e:
            print("[Erreur] Une erreur s'est produite lors de la modification de l'exposition permanenete :", e)
    else:
        print("[WARN] Veuillez entrer un entier entre 1 et 2")
        return


def update_exposition_temporaire(cur,connexion):
    print("---------Mettre à jour de l'oeuvre temporaire----------")
    test = False
    while not test:
        nom = input("Le nom de l'exposition temporaire à modifier : ")
        sql = f"SELECT nom FROM ExpositionTemporaire WHERE nom='{nom}';"
        cur.execute(sql)
        if cur.fetchone():
            test = True
        else:
            print(f"[WARN] L'exposition temporaire {nom} n'existe pas")
    choix = input("1 ---- changer date début\n2 ---- changer date fin\n3 ---- ajouter une salle\n4 ---- supprimmer une salle\n5 ---- ajouter un panneau explicatif\n6 ---- supprimmer un panneau explicatif\n7 ---- ajouter un guide\n8 ---- supprimmer un guide\nvotre choix : ")
    if choix == "1":
        date_debut = input("Nouvelle Date debut(DD/MM/YYYY): ")
        # Verifier le format de la date debut
        while(validate_date_2(date_debut)==False) :
            print("[WARN] Le format de date debut ne suffit pas!")
            date_debut = input("Nouvelle Date debut(DD/MM/YYYY): ")
        sql = f"UPDATE expositiontemporaire SET date_debut = '{date_debut}' WHERE nom='{nom}';"
    elif choix == "2":
        date_fin = input("Nouvelle Date fin(DD/MM/YYYY): ")
        # Verifier le format de la date fin
        while(validate_date_2(date_fin)==False) :
            print("[WARN] Le format de date fin ne suffit pas!")
            date_debut = input("Nouvelle Date fin(DD/MM/YYYY): ")
        sql = f"UPDATE expositiontemporaire SET date_fin = '{date_fin}' WHERE nom='{nom}';"
    elif choix == "3":
        sql = f"SELECT et.salle FROM expositiontemporaire et WHERE nom='{nom}';"
        cur.execute(sql)
        salles = cur.fetchone()[0]
        salle_capacite = input("Capacité de salle : ")
        while not salle_capacite or int(salle_capacite) <= 0:
            print("[WARN] Capacité de salle doit > 0")
            salle_capacite = input("Capacité de salle : ")
        salle = {
            "num": salles[len(salles)-1]["num"]+1,
            "capacite": int(salle_capacite),
        }
        salles.append(salle)
        salle_json = json.dumps(salles)
        sql = f"UPDATE expositiontemporaire SET salle = '{salle_json}' WHERE nom='{nom}';"
    elif choix == "4":
        sql = f"SELECT et.salle FROM expositiontemporaire et WHERE nom='{nom}';"
        cur.execute(sql)
        salles = cur.fetchone()
        print(salles)
        num = input("Le numéro de salle que vous voulez supprimer: ")
        while 1:
            try:
                num = int(num)
                if num >= 0:
                    break
                else:
                    print("[WARN] Le numéro de salle doit > 0")
                    num = input("Le numéro de salle que vous voulez supprimer: ")
            except ValueError:
                print("[WARN] Entrez un nombre!")
                num = input("Le numéro de salle que vous voulez supprimer: ")
        salles = [salle for salle in salles[0] if salle["num"] != num]
        salle_json = json.dumps(salles)
        sql = f"UPDATE expositiontemporaire SET salle = '{salle_json}' WHERE nom='{nom}';"
    elif choix == "5":
        sql = f"SELECT et.panneauexplicatif FROM expositiontemporaire et WHERE nom='{nom}';"
        cur.execute(sql)
        panneaux = cur.fetchone()[0]
        panneau_texte = input("Texte de panneau explicatif: ")
        while len(panneau_texte) == 0:
            print("[WARN] Texte du panneau NOT NULL")
            panneau_texte = input("Texte de panneau explicatif: ")
        panneau = {
            "num": salles[len(salles)-1]["num"]+1,
            "texte": panneau_texte, 
        }
        panneaux.append(panneau)
        panneaux_json = json.dumps(panneaux)
        sql = f"UPDATE expositiontemporaire SET panneauexplicatif = '{panneaux_json}' WHERE nom='{nom}';"
    elif choix == "6":
        sql = f"SELECT et.panneauexplicatif FROM expositiontemporaire et WHERE nom='{nom}';"
        cur.execute(sql)
        panneaux = cur.fetchone()
        print(panneaux)
        num = input("Le numéro de panneau que vous voulez supprimer: ")
        while 1:
            try:
                num = int(num)
                if num >= 0:
                    break
                else:
                    print("[WARN] Le numéro de panneau doit > 0")
                    num = input("Le numéro de panneau que vous voulez supprimer: ")
            except ValueError:
                print("[WARN] Entrez un nombre!")
                num = input("Le numéro de panneau que vous voulez supprimer: ")
        panneaux = [panneau for panneau in panneaux[0] if panneau["num"] != num]
        panneaux_json = json.dumps(panneaux)
        sql = f"UPDATE expositiontemporaire SET panneauexplicatif = '{panneaux_json}' WHERE nom='{nom}';"
    elif choix == "7":
        sql = f"SELECT et.guide FROM expositiontemporaire et WHERE nom='{nom}';"
        cur.execute(sql)
        row = cur.fetchone()
        guides = row[0]
        guide_nom = input("Nom de guide : ")
        while len(guide_nom) == 0:
            print("[WARN] Nom de guide Non NULL!")
            guide_nom = input("Nom de guide : ")
        
        guide_prenom = input("Prénom de guide : ")
        while len(guide_prenom) == 0:
            print("[WARN] Prénom de guide Non NULL!")
            guide_prenom = input("Prénom de guide : ")

        guide_adresse = input("Adresse de guide : ")
        while len(guide_prenom) == 0:
            print("[WARN] Adresse de guide Non NULL!")
            guide_adresse = input("Adresse de guide : ")

        guide_date_embauche = input("Date embauché de guide (DD/MM/YYYY): ")
        while(validate_date_2(guide_date_embauche) == False):
            print("[WARN] Le format de Date embauché de guide n'est pas bon!")
            guide_date_embauche = input("Date embauché de guide (DD/MM/YYYY): ")

        while len(guide_prenom) == 0:
            print("[WARN] Date embauché de guide Non NULL!")
            guide_date_embauche = input("Date embauché de guide : ")

        guide = {
            "nom": guide_nom,
            "prenom": guide_prenom,
            "adresse": guide_adresse,
            "date_embauche": guide_date_embauche,
        }
        guides.append(guide)
        json_guides = json.dumps(guides)
        sql = f"UPDATE expositiontemporaire SET guide = '{json_guides}' WHERE nom='{nom}';"
    elif choix == "8":
        # 
        sql = f"SELECT et.guide FROM expositiontemporaire et WHERE nom='{nom}';"
        cur.execute(sql)
        row = cur.fetchone()
        guides = row[0]
        guide_nom = input("Nom de guide que vous voulez supprimer: ")
        while len(guide_nom) == 0:
            print("[WARN] Nom de guide Non NULL!")
            guide_nom = input("Nom de guide que vous voulez supprimer: ")
        
        guide_prenom = input("Prénom de guide que vous voulez supprimer: ")
        while len(guide_prenom) == 0:
            print("[WARN] Prénom de guide Non NULL!")
            guide_prenom = input("Prénom de guide que vous voulez supprimer: ")
        index_delete = -1
        index = 0
        for guide in guides:
            if guide["nom"] == guide_nom and guide["prenom"] == guide_prenom:
                index_delete = index
                break
            index += 1
        if index_delete != -1:
            del guides[index_delete]
        json_guides = json.dumps(guides)
        sql = f"UPDATE expositiontemporaire SET guide = '{json_guides}' WHERE nom='{nom}';"
    else:
        print("[WARN] Veuillez entrer un entier entre 1 et 8")
        return
    try:
        cur.execute(sql)
        connexion.commit()
        print("[INFO] L'exposition temporaire a été modifiée avec succès.")
    except psycopg2.DataError as e:
        print("[Erreur] Une erreur s'est produite lors de la modification de l'exposition temporaire :", e)
    
        