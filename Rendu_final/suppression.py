import json
import psycopg2

# test
connexion = psycopg2.connect("dbname ='dbnf18p049' user='nf18p049' password='sbX7OFf3sl77' host='tuxa.sme.utc'")
cur = connexion.cursor()


def delete_oeuvre(cur, connexion):
    oeuvre_titre = input("Le titre de l'oeuvre que vous voulez supprimmer")
    try:
        cur.execute("DELETE FROM Emprunte WHERE oeuvre = %s", (oeuvre_titre,))
        cur.execute("DELETE FROM Oeuvre WHERE titre = %s", (oeuvre_titre,))
        connexion.commit()
        print("[INFO] L'oeuvre a été supprimé avec succès.")
    except psycopg2.Error as e:
        connexion.rollback()
        print("[ERREUR] Une erreur s'est produite lors de supprimmation de l'oeuvre : ", e)

def delete_auteur(cur, connexion):
    auteur_id = input("L'identifiant de l'auteur que vous voulez supprimmer (cela supprimera toutes ses oeuvres et les emprunts concernant cet auteur): ")
    try:
        cur.execute("SELECT titre FROM Oeuvre WHERE auteur = %s", (auteur_id,))
        rows = cur.fetchall()
        for row in rows:
            cur.execute("DELETE FROM Emprunte WHERE oeuvre = %s", (row[0],))
            cur.execute("DELETE FROM Oeuvre WHERE titre = %s", (row[0],))
        cur.execute("DELETE FROM Auteur WHERE id = %s", (auteur_id,))
        connexion.commit()
        print("[INFO] L'auteur a été supprimé avec succès.")
    except psycopg2.Error as e:
        connexion.rollback()
        print("[ERREUR] Une erreur s'est produite lors de supprimation de l'auteur : ", e)

def delete_musee(cur, connexion):
    musee_id = input("Le nom de musée que vous voulez supprimer : ")
    try:
        cur.execute("UPDATE Oeuvre SET musee = NULL WHERE musee = %s", (musee_id,))
        cur.execute("DELETE FROM Emprunte WHERE musee = %s", (musee_id,))
        cur.execute("DELETE FROM Musee WHERE nom = %s", (musee_id,))
        connexion.commit()
        print("[INFO] L'musée a été supprimé avec succès.")
    except (Exception, psycopg2.Error) as error:
        connexion.rollback()
        print("[ERREUR] Une erreur s'est produite lors de supprimation de l'auteur : ", error)

def delete_exposition(cur, connexion):
    exposition_id = input("Le nom d'exposition que vous voulez supprimer : ")
    try:
        cur.execute("UPDATE Oeuvre SET exposition = NULL WHERE exposition = %s", (exposition_id,))
        cur.execute("DELETE FROM ExpositionTemporaire WHERE nom = %s", (exposition_id,))
        cur.execute("DELETE FROM ExpositionPermanente WHERE nom = %s", (exposition_id,))
        cur.execute("DELETE FROM Exposition WHERE nom = %s", (exposition_id,))
        connexion.commit()
        print("[INFO] L'exposition a été supprimé avec succès.")
    except (Exception, psycopg2.DatabaseError) as error:
        connexion.rollback()
        print("[ERREUR] Une erreur s'est produite lors de supprimation de l'exposition : ", error)

def delete_emprunte(cur, connexion):
    musee = input("Le nom de musée de l'emprunte que vous voulez supprimmer : ")
    oeuvre = input("Le titre de l'oeuvre de l'emprunte que vous voulez supprimmer : ")
    try:
        cur.execute("DELETE FROM emprunte WHERE musee=%s and oeuvre=%s", (musee,oeuvre))
        connexion.commit()
        print("[INFO] L'emprunte a été supprimé avec succès.")
    except psycopg2.Error as e:
        connexion.rollback()
        print("[ERREUR] Une erreur s'est produite lors de supprimation de l'emprunte : ", e)