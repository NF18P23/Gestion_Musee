from ajout import create_auteur, create_compte, create_musee, create_exposition_permanente, create_exposition_temporaire, create_oeuvre, create_emprunte
from update import update_musee, update_auteur, update_emprunte, update_oeuvre, update_exposition_permanente, update_exposition_temporaire
from lecture import lecture_auteur, lecture_musee, lecture_exposition, lecture_exposition_permanente, lecture_exposition_temporaire, lecture_oeuvre, lecture_emprunte
from suppression import delete_oeuvre, delete_auteur, delete_emprunte, delete_exposition, delete_musee
import json
import psycopg2

connexion = psycopg2.connect("dbname ='dbnf18p049' user='nf18p049' password='sbX7OFf3sl77' host='tuxa.sme.utc'")
cur = connexion.cursor()

def authentication(login, password):
    sql = "SELECT role FROM Comptes WHERE id='%s'AND mdp='%s'" % (login,password)
    cur.execute(sql)
    row = cur.fetchone()
    if row:
        return row[0]
    else:
        return ''

def login():
    login = input("Login : ")
    passwd = input("Password : ")
    group = authentication(login, passwd)
    if group == '':
        print("[ERROR] Authentication failed")
        return
    return group

def display_menu_admin():
    print("--------------------------------MENU--------------------------------")
    print("Bienvenue dans le menu de gestion des musées")
    print("Entrez 0 pour quitter")
    print("Entrez 1 pour lire des données")
    print("Entrez 2 pour ajouter des données")
    print("Entrez 3 pour mettre à jour des données")
    print("Entrez 4 pour supprimer des données")

def display_menu_utilisateur():
    print("--------------------------------MENU--------------------------------")
    print("Bienvenue dans le menu de gestion des musées")
    print("Entrez 0 pour quitter")
    print("Entrez 1 pour lire des données")

def display_menu_creation():
    print("--------------------------------CREATE--------------------------------")
    print("Entrez 1 pour ajouter un auteur")
    print("Entrez 2 pour ajouter un musée")
    print("Entrez 3 pour ajouter un exposition temporaire")
    print("Entrez 4 pour ajouter un exposition permanente")
    print("Entrez 5 pour ajouter un oeuvre")
    print("Entrez 6 pour ajouter un emprunte")
    print("Entrez 7 pour ajouter un compte")

def creation():
    display_menu_creation()
    choice = input("Votre choix : ")
    if choice == '1':
        create_auteur(cur,connexion)
    elif choice == '2':
        create_musee(cur,connexion)
    elif choice == '3':
        create_exposition_temporaire(cur,connexion)
    elif choice == '4':
        create_exposition_permanente(cur,connexion)
    elif choice == '5':
        create_oeuvre(cur,connexion)
    elif choice == '6':
        create_emprunte(cur,connexion)
    elif choice == '7':
        create_compte(cur,connexion)
    else:
        print("[WARN] Choix invalid")
        return

def display_menu_update():
    print("--------------------------------UPDATE--------------------------------")
    print("Entrez 1 pour mettre à jour d'un auteur")
    print("Entrez 2 pour mettre à jour d'un emprunte")
    print("Entrez 3 pour mettre à jour d'un oeuvre")
    print("Entrez 4 pour mettre à jour d'un musée")
    print("Entrez 5 pour mettre à jour d'un exposition temporaire")
    print("Entrez 6 pour mettre à jour d'un exposition permanente")
    print("Entrez 7 pour mettre à jour d'un compte")

def update():
    display_menu_update()
    choice = input("Votre choix : ")
    if choice == '1':
        update_auteur(cur,connexion)
    elif choice == '2':
        update_emprunte(cur,connexion)
    elif choice == '3':
        update_oeuvre(cur,connexion)
    elif choice == '4':
        update_musee(cur,connexion)
    elif choice == '5':
        update_exposition_temporaire(cur,connexion)
    elif choice == '6':
        update_exposition_permanente(cur,connexion)
    else:
        print("[WARN] Choix invalid")
        return

def display_menu_lecture():
    print("--------------------------------READ--------------------------------")
    print("Entrez 1 pour lire tous les auteurs")
    print("Entrez 2 pour lire tous les empruntes")
    print("Entrez 3 pour lire tous les oeuvre")
    print("Entrez 4 pour lire tous les musées")
    print("Entrez 5 pour lire tous les expositions temporaires")
    print("Entrez 6 pour lire tous les expositions permanentes")
    print("Entrez 7 pour lire tous les expositions")

def lecture():
    display_menu_lecture()
    choice = input("Votre choix : ")
    if choice == '1':
        lecture_auteur(cur,connexion)
    elif choice == '2':
        lecture_emprunte(cur,connexion)
    elif choice == '3':
        lecture_oeuvre(cur,connexion)
    elif choice == '4':
        lecture_musee(cur,connexion)
    elif choice == '5':
        lecture_exposition_temporaire(cur,connexion)
    elif choice == '6':
        lecture_exposition_permanente(cur,connexion)
    elif choice == '7':
        lecture_exposition(cur,connexion)
    else:
        print("[WARN] Choix invalid")
        return

def display_menu_delete():
    print("--------------------------------DELETE--------------------------------")
    print("Entrez 1 pour supprimer un auteur")
    print("Entrez 2 pour supprimer un musée")
    print("Entrez 3 pour supprimer un exposition")
    print("Entrez 4 pour supprimer un oeuvre")
    print("Entrez 5 pour supprimer un emprunte")

def delete():
    display_menu_delete()
    choice = input("Votre choix : ")
    if choice == '1':
        delete_auteur(cur,connexion)
    elif choice == '2':
        delete_musee(cur,connexion)
    elif choice == '3':  
        delete_exposition(cur,connexion)
    elif choice == '4':
        delete_oeuvre(cur,connexion)
    elif choice == '5':
        delete_emprunte(cur,connexion)
    else:
        print("[WARN] Choix invalid")
        return

def main():
    group = login()
    if group == 'admin' :
        while True:
            display_menu_admin()
            choice = input("Qu'est-ce que vous voulez faire? ")
            if choice == '0':
                print("À bientôt!")
                connexion.close()
                exit(1)
            elif choice == '1':
                lecture()
            elif choice == '2':
                creation()
            elif choice == '3':
                update()
            elif choice == '4':
                delete()
            else:
                print("[WARN] Choix invalid")
    elif group == 'utilisateur':
        while True:
            display_menu_utilisateur()
            choice = input("Qu'est-ce que vous voulez faire? ")
            if choice == '0':
                print("À bientôt!")
                connexion.close()
                return
            elif choice == '1':
                lecture()

main()

