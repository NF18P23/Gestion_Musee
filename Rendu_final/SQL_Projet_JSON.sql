DROP TABLE IF EXISTS Emprunte;
DROP TABLE IF EXISTS Oeuvre;
DROP TABLE IF EXISTS ExpositionPermanente;
DROP TABLE IF EXISTS ExpositionTemporaire;
DROP TABLE IF EXISTS Exposition;
DROP TABLE IF EXISTS Musee;
DROP TABLE IF EXISTS Auteur;
DROP TABLE IF EXISTS Comptes;
DROP TYPE IF EXISTS role_compte;
DROP TYPE IF EXISTS type_oeuvre;


CREATE TABLE Musee(
	nom VARCHAR PRIMARY KEY,
	adresse JSON NOT NULL
);

CREATE TABLE Auteur(
	id INTEGER PRIMARY KEY ,
	nom VARCHAR NOT NULL,
	prenom VARCHAR NOT NULL,
	date_naissance DATE,
	date_mort DATE,
	CHECK(date_mort>date_naissance)
);

CREATE TABLE Exposition(
	nom VARCHAR PRIMARY KEY
);

CREATE TABLE ExpositionTemporaire(
	nom VARCHAR PRIMARY KEY,
	date_debut DATE NOT NULL,
	date_fin DATE NOT NULL,
	salle JSON NOT NULL, -- [{"num" : "...", "capacite" : "..."},{"num" : "...", "capacite" : "..."},...]
	PanneauExplicatif JSON NOT NULL, -- {"num" : "...", "texte" : "..."}
	guide JSON , -- [{"nom" : "...", "prenom" : "...", "adresse" : "...", "date_embauche" : "..."},
											 --  {"nom" : "...", "prenom" : "...", "adresse" : "...", "date_embauche" : "..."},
											 --   ...]
	FOREIGN KEY (nom) REFERENCES Exposition(nom),
	CHECK(date_fin>date_debut)
);

CREATE TABLE ExpositionPermanente(
	nom VARCHAR PRIMARY KEY,
	guide JSON, -- [{"nom" : "...", "prenom" : "...", "adresse" : "...", "date_embauche" : "...", "creneau" : [{"jour" : "...", "heure_debut" : "...", "heure_fin" :"..."},
  										 --     																																												{"jour" : "...", "heure_debut" : "...", "heure_fin" :"..."},
											 --																																															...]},
											 -- [{"nom" : "...", "prenom" : "...", "adresse" : "...", "date_embauche" : "...", "creneau" : [{"jour" : "...", "heure_debut" : "...", "heure_fin" :"..."},
										   --     																																												{"jour" : "...", "heure_debut" : "...", "heure_fin" :"..."},
										 	 --																																															...]},
										 	 --   ...]
	FOREIGN KEY (nom) REFERENCES Exposition(nom)
);

CREATE TYPE type_oeuvre AS ENUM ('peinture','sculpture','photo');
CREATE TABLE Oeuvre (
    titre VARCHAR PRIMARY KEY,
    type type_oeuvre,
    date DATE,
    dimension JSON,
    prix_acquisition INTEGER,
    restauree BOOLEAN,
    auteur INTEGER NOT NULL,
		exposition VARCHAR,
		musee VARCHAR,
    FOREIGN KEY (exposition) REFERENCES Exposition(nom),
		FOREIGN KEY (musee) REFERENCES Musee(nom),
		FOREIGN KEY (auteur) REFERENCES Auteur(id),
    CHECK(prix_acquisition > 0)
);

CREATE TABLE Emprunte(
	musee VARCHAR,
	oeuvre VARCHAR ,
	date_debut DATE,
	date_fin DATE,
	PRIMARY KEY (musee,oeuvre),
	FOREIGN KEY (musee) REFERENCES Musee(nom),
	FOREIGN KEY(oeuvre) REFERENCES Oeuvre(titre)
);

CREATE TYPE role_compte AS ENUM('utilisateur','admin');
CREATE TABLE Comptes(
	id VARCHAR PRIMARY KEY,
	mdp VARCHAR NOT NULL,
	role role_compte NOT NULL
);

INSERT INTO Musee VALUES ('Le Louvre', '{"rue": "Rue de Rivoli", "postcode" : "75001", "ville" : "Paris"}');
INSERT INTO Musee VALUES ('Centre Pompidou', '{"rue": "Place Georges-Pompidou", "postcode" : "75004", "ville" : "Paris"}');

INSERT INTO Auteur VALUES(1,'Van Gogh','Vincent',TO_DATE('30/03/1853', 'DD/MM/YYYY'),TO_DATE('29/07/1890', 'DD/MM/YYYY'));

INSERT INTO Auteur VALUES(2 ,'Monet','Claude',  TO_DATE('14/11/1840', 'DD/MM/YYYY'),TO_DATE('19/12/1926', 'DD/MM/YYYY'));

INSERT INTO Auteur VALUES(3 ,'Michelangelo','Buonarroti',  TO_DATE('06/03/1475', 'DD/MM/YYYY'),TO_DATE('18/02/1564', 'DD/MM/YYYY'));

INSERT INTO Auteur VALUES(4 ,'Banksy',' ',  TO_DATE('28/07/1974', 'DD/MM/YYYY'),NULL);

INSERT INTO Auteur VALUES(5 ,'Belin','Valérie',  TO_DATE('03/02/1964', 'DD/MM/YYYY'),NULL);

INSERT INTO Auteur VALUES(6 ,'Rodin','Auguste',  TO_DATE('12/11/1840', 'DD/MM/YYYY'),TO_DATE('17/11/1917', 'DD/MM/YYYY'));

INSERT INTO Exposition VALUES ('Impressionnisme');

INSERT INTO Exposition VALUES ('Les images intranquilles');

INSERT INTO Exposition VALUES ('Noir & Blanc');

INSERT INTO Exposition VALUES ('Les Choses. Une histoire de la nature morte');

INSERT INTO Exposition VALUES ('Le Corps et l Ame');

INSERT INTO Oeuvre VALUES('Impression, soleil levant', 'peinture', '01-04-1874'::date, '{"length" : 1, "width" : 350}', NULL, 'TRUE', 2, 'Impressionnisme', 'Le Louvre');

INSERT INTO Oeuvre VALUES('Les Coquelicots', 'peinture', '01-04-1873'::date, '{"length" : 50, "width" : 65}', NULL, 'TRUE', 2, 'Impressionnisme', 'Le Louvre');

INSERT INTO Oeuvre VALUES('La nuit étoilé', 'peinture', '01/06/1889'::date, '{"length" : 50, "width" : 65}', NULL, 'TRUE', 1, 'Impressionnisme' , 'Le Louvre');

INSERT INTO Oeuvre VALUES('Champ de blé aux corbeaux', 'peinture', TO_DATE('01/07/1890', 'DD/MM/YYYY'), '{"length" : 50, "width" : 65}', NULL, 'TRUE', 1, 'Impressionnisme' , 'Le Louvre');

INSERT INTO Oeuvre VALUES('Les tournesols', 'peinture', TO_DATE('01/08/1888', 'DD/MM/YYYY'), '{"length" : 50, "width" : 65}', NULL, 'TRUE', 1, 'Impressionnisme' , 'Le Louvre');

INSERT INTO Oeuvre VALUES('David', 'sculpture', '01/01/1504'::date, '{"length" : 50, "width" : 65}', NULL, 'TRUE', 3,'Le Corps et l Ame' , 'Le Louvre');

INSERT INTO Oeuvre VALUES('L’esclave Rebelle', 'sculpture', '01/01/1516'::date, '{"length" : 50, "width" : 65}', NULL, 'TRUE', 3,'Le Corps et l Ame' , 'Le Louvre');

INSERT INTO Oeuvre VALUES('La petite fille au ballon', 'peinture', '01/01/2002'::date, '{"length" : 50, "width" : 65}', NULL, 'TRUE', 4, 'Impressionnisme' , 'Le Louvre');

INSERT INTO Oeuvre VALUES('Bodybuilders', 'photo', '01-01-1999'::date, '{"length" : 50, "width" : 65}', NULL, 'TRUE', 5, 'Les images intranquilles', 'Centre Pompidou');

INSERT INTO Oeuvre VALUES('Still Life with Dish', 'photo', '05-06-2014'::date, '{"length" : 50, "width" : 65}', 1000, 'FALSE', 5, 'Les Choses. Une histoire de la nature morte', 'Le Louvre');

INSERT INTO Oeuvre VALUES('Mannequins', 'photo', '03-04-2003'::date, '{"length" : 50, "width" : 65}', 1500, 'FALSE', 5,'Noir & Blanc', 'Le Louvre');

INSERT INTO Oeuvre VALUES('Baiser', 'sculpture', '01-01-1900'::date, '{"length" : 50, "width" : 65}', 1500, 'FALSE', 6,'Le Corps et l Ame', 'Le Louvre');


INSERT INTO Emprunte VALUES ('Le Louvre', 'Bodybuilders', TO_DATE('12/12/2022', 'DD/MM/YYYY'), TO_DATE('10/06/2023', 'DD/MM/YYYY'));
INSERT INTO Emprunte VALUES ('Centre Pompidou', 'Mannequins', TO_DATE('12/12/2022', 'DD/MM/YYYY'), TO_DATE('10/06/2023', 'DD/MM/YYYY'));

INSERT INTO ExpositionPermanente VALUES ('Impressionnisme','[{"nom" : "Dupont", "prenom" : "Jean","adresse" : "rue Gambetta Paris", "date_embauche" : "2019-06-01",
																															"creneau" : [{"jour" : "mardi", "heure_debut" : "10:00:00", "heure_fin" : "12:00:00"}]},
																														{"nom" : "Schmidt", "prenom" : "Valerie","adresse" : "rue Mouchotte Compiègne", "date_embauche" : "2019-06-01",
																														 "creneau" : [{"jour" : "Mercredi", "heure_debut" : "14:00:00", "heure_fin" : "16:00:00"}]}
																													  ]');
INSERT INTO ExpositionPermanente VALUES ('Noir & Blanc', '[{"nom" : "Schmidt", "prenom" : "Valerie","adresse" : "rue Mouchotte Compiègne", "date_embauche" : "2019-06-01",
																													 "creneau" : [{"jour" : "lundi", "heure_debut" : "13:00:00", "heure_fin" : "15:00:00"}]},
																													{"nom" : "Martin", "prenom" : "Marie","adresse" : "rue d’Argentine Paris", "date_embauche" : "2020-02-02",
																													 "creneau" : [{"jour" : "lundi", "heure_debut" : "11:00:00", "heure_fin" : "13:00:00"}]}
																													  ]');
INSERT INTO ExpositionPermanente VALUES ('Le Corps et l Ame');

INSERT INTO ExpositionTemporaire VALUES ('Les images intranquilles',TO_DATE('03/04/2015', 'DD/MM/YYYY'),TO_DATE('03/05/2015', 'DD/MM/YYYY'),
																				'[{"num" : 1, "capacite" : 350}]',
																				'[{"num" : 2, "texte" : "Description"}]',
																				'[{"nom" : "Martin", "prenom" : "Marie","adresse" : "rue d’Argentine Paris", "date_embauche" : "2020-02-02"}]'
																				 );
INSERT INTO ExpositionTemporaire VALUES ('Les Choses. Une histoire de la nature morte',TO_DATE('12/10/2022', 'DD/MM/YYYY'),TO_DATE('23/01/2023', 'DD/MM/YYYY'),
																					'[{"num" : 2, "capacite" : 450},{"num" : 3, "capacite" : 300}]',
																					'[{"num" : 1, "texte" : "Histoire"}]'
																					);

INSERT INTO Comptes VALUES('utilisateur','123','utilisateur');
INSERT INTO Comptes VALUES('admin','123','admin');



SELECT titre FROM Oeuvre
	JOIN Auteur ON Auteur.id=Oeuvre.auteur
	WHERE Auteur.nom = 'Van Gogh';

SELECT type, count(*) AS Nombre_Oeuvres
FROM Oeuvre o
WHERE musee = 'Le Louvre'
GROUP BY type;

SELECT Auteur.nom, Auteur.prenom, count(*) AS Nombre_Oeuvres
FROM Auteur JOIN Oeuvre ON Auteur.id = Oeuvre.auteur
WHERE Auteur.date_mort IS NULL AND Oeuvre.musee = 'Le Louvre'
GROUP BY Auteur.id
ORDER BY Auteur.nom;


SELECT titre,Oeuvre.musee FROM Oeuvre
JOIN Emprunte ON Emprunte.oeuvre=Oeuvre.titre
WHERE Emprunte.musee='Le Louvre';

SELECT nom, count(*) AS Nb_Salles, SUM(CAST(s->>'capacite' AS INTEGER)) AS Capacite
FROM ExpositionTemporaire e, JSON_ARRAY_ELEMENTS(e.salle) s
GROUP BY nom;


SELECT e.nom AS expo, g->>'nom' AS Nom , g->>'prenom' AS Nom, c->>'jour' AS Jour, c->>'heure_debut' AS HeureDebut
FROM ExpositionPermanente e, JSON_ARRAY_ELEMENTS(e.guide) g, JSON_ARRAY_ELEMENTS(CAST(g->>'creneau' AS JSON)) c;
