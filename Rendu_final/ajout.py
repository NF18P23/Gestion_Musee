from datetime import datetime
import json
import psycopg2
import re
from lecture import lecture_auteur, lecture_musee, lecture_exposition, lecture_exposition_permanente, lecture_exposition_temporaire, lecture_oeuvre, lecture_emprunte
# # test
# connexion = psycopg2.connect("dbname ='dbnf18p049' user='nf18p049' password='sbX7OFf3sl77' host='tuxa.sme.utc'")
# cur = connexion.cursor()



# validate_date 1
def validate_date_1(date_text):
    try:
        if date_text != datetime.strptime(date_text, "%d-%m-%Y").strftime('%d-%m-%Y'):
            raise ValueError
        return True
    except ValueError:
        return False

# validate_date 2
def validate_date_2(date_text):
    try:
        if date_text != datetime.strptime(date_text, "%d/%m/%Y").strftime('%d/%m/%Y'):
            raise ValueError
        return True
    except ValueError:
        return False

def validate_time_format(time_str):
    pattern = r'^([0-1][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$'
    match = re.match(pattern, time_str)
    
    if match:
        hour = int(match.group(1))
        minute = int(match.group(2))
        second = int(match.group(3))
        
        if hour <= 23 and minute <= 59 and second <= 59:
            return True
    
    return False

# creation de Auteur
def create_auteur(cur, connexion):
    print("---------creation d'un auteur----------")
    # afficher tous les auteurs pour l'instant
    sql = "SELECT MAX(id) FROM Auteur"
    cur.execute(sql)
    row = cur.fetchone()
    while(row) :
        num = row[0] + 1
        row = cur.fetchone()
    row = 1
    # le nom de l'auteur NOT NULL
    nom = ""
    while len(nom) == 0:
        nom = input("Nom de l'auteur: ")
        if len(nom) == 0:
            print("[WARN] Nom ne peut pas être vide!")

    # le prenom de l'auteur NOT NULL
    prenom = ""
    while len(prenom) == 0:
        prenom = input("Prénom de l'auteur: ")
        if len(prenom) == 0:
            print("[WARN] Prénom ne peut pas être vide!")

    # validate_date
    date_naissance = input("Date de naissance(DD-MM-YYYY): ")
    while (validate_date_1(date_naissance)==False) :
        print("[WARN] Le format de la date de naissance(DD-MM-YYYY)!")
        date_naissance = input("Date de naissance(DD-MM-YYYY): ")

    # date de mort
    choix = input("Est-il mort ? (oui :o) : ")
    date_mort = ""
    if (choix=="o") :
        while (validate_date_1(date_mort)==False) :
            date_mort = input("Date de mort(DD-MM-YYYY): ")

    # Verification si l'auteur existe deja dans BD
    sql = f"SELECT * FROM Auteur WHERE nom='{nom}' AND prenom='{prenom}';"
    cur.execute(sql)
    if cur.fetchone():
        print(f"[ERREUR] L'auteur {nom} {prenom} existe déjà")
    else:
        # Insertion du auteur dans la base de données
        try:
            if (date_mort):
                cur.execute(
                    "INSERT INTO Auteur VALUES (%s, %s, %s, TO_DATE(%s,'DD/MM/YYYY'), TO_DATE(%s,'DD/MM/YYYY'))",
                    (num, nom, prenom, date_naissance, date_mort)
                )
                connexion.commit()
            else:
                cur.execute(
                    "INSERT INTO Auteur VALUES (%s, %s, %s, TO_DATE(%s,'DD/MM/YYYY'), NULL)",
                    (num, nom, prenom, date_naissance)
                )
                connexion.commit()
            print("[INFO] L'auteur a été créé avec succès.")
        except psycopg2.Error as e:
            print("[ERREUR] Une erreur s'est produite lors de la création du auteur :", e)

# creation du musee
def create_musee(cur, connexion):
    print("---------creation d'un musee----------")
    # Nom du musee
    nom = ""
    while len(nom) == 0:
        nom = input("Nom du musee: ")
        if len(nom) == 0:
            print("[WARN] Nom ne peut pas être vide!")
    # Verification le musee deja existe
    cur.execute("SELECT COUNT(*) FROM Musee WHERE nom = %s", (nom,))
    count = cur.fetchone()[0]
    if count > 0 :
        print("[ERREUR] Le muséé existe déjà dans la base de données.")
        return
    # adresse du musee
    rue = ""
    while len(rue) == 0:
        rue = input("Rue: ")
        if len(rue) == 0:
            print("[WARN] rue ne peut pas être vide!")
    postcode = ""
    while len(postcode) == 0:
        postcode = input("Postcode: ")
        if len(postcode) == 0:
            print("[WARN] postcode ne peut pas être vide!")
    ville = ""
    while len(ville) == 0:
        ville = input("Ville: ")
        if len(ville) == 0:
            print("[WARN] ville ne peut pas être vide!")
    adresse = {
        "rue": rue,
        "postcode": postcode,
        "ville": ville,
    }
    adresse = json.dumps(adresse)
    # ajouter le musee
    try:
        cur.execute("INSERT INTO Musee VALUES (%s, %s)", (nom, adresse))
        connexion.commit()
        print("[INFO] Le musee a été créé avec succès.")
    except psycopg2.Error as e:
        print("[ERREUR] Une erreur s'est produite lors de la création du exposition :", e)
        return

# creation du Exposition temporaire
def create_exposition_temporaire(cur, connexion):
    print("---------creation d'un exposition temporaire----------")
    # nom du exposition permanent
    nom = ""
    while len(nom) == 0:
        nom = input("Nom du exposition temporaire: ")
        cur.execute("SELECT * FROM Exposition WHERE nom = '%s'" % nom)
        row = cur.fetchone()
        # Verification l'exposition deja existe
        if row:
            print("[ERREUR] L'exposition temporaire existe déjà dans la base de données.")
            return
    date_debut = input("Date debut(DD/MM/YYYY): ")
    # Verifier le format de la date debut
    while(validate_date_2(date_debut)==False) :
        print("[WARN] Le format de date debut ne suffit pas!")
        date_debut = input("Date debut(DD/MM/YYYY): ")
    date_fin = input("Date fin(DD/MM/YYYY): ")
    # Verifier le format de la date fin
    while(validate_date_2(date_fin)==False) :
        print("[WARN] Le format de date fin ne suffit pas!")
        date_fin = input("Date fin(DD/MM/YYYY): ")

    # Salle
    nb_salle = input("Combien de salle servant à l'exposition : ")
    while not nb_salle or int(nb_salle) <= 0:
        print("[WARN] Nombre de salle doit > 0 et Non NULL")
        nb_salle = input("Combien de salle servant à l'exposition : ")
    count = 0
    salles = []
    while (count<int(nb_salle)):
        salle_capacite = input("Capacité de salle : ")
        while not salle_capacite or int(salle_capacite) <= 0:
            print("[WARN] Capacité de salle doit > 0")
            salle_capacite = input("Capacité de salle : ")
        salle = {
            "num": count,
            "capacite": int(salle_capacite),
        }
        count += 1
        salles.append(salle)
    salles_json = json.dumps(salles)

    # panneau explicatif
    nb_panneau = input("Combien de panneau explicatif servant à l'exposition : ")
    while not nb_panneau or int(nb_panneau) <= 0:
        print("[WARN] Nombre de panneau explicatif doit > 0")
        nb_panneau = input("Combien de panneau servant à l'exposition : ")
    count = 0
    panneaux = []
    while (count<int(nb_panneau)):
        panneau_texte = input("Texte de panneau explicatif: ")
        while len(panneau_texte) == 0:
            print("[WARN] Texte du panneau NOT NULL")
            panneau_texte = input("Texte de panneau explicatif: ")
        panneau = {
            "num": count,
            "texte": panneau_texte, 
        }
        count += 1
        panneaux.append(panneau)
    panneaux_json = json.dumps(panneaux)

    # guide
    nb_guide = input("Combien de guide qui est responsable de cette exposition: ")
    while not nb_guide or int(nb_guide) <= 0:
        print("[WARN] Nombre de guide doit > 0")
        nb_guide = input("Combien de guide qui est responsable de cette exposition: ")
    count = 0
    guides = []
    while (count<int(nb_guide)):
        guide_nom = input("Nom de guide : ")
        while len(guide_nom) == 0:
            print("[WARN] Nom de guide Non NULL!")
            guide_nom = input("Nom de guide : ")
        
        guide_prenom = input("Prénom de guide : ")
        while len(guide_prenom) == 0:
            print("[WARN] Prénom de guide Non NULL!")
            guide_prenom = input("Prénom de guide : ")

        guide_adresse = input("Adresse de guide : ")
        while len(guide_prenom) == 0:
            print("[WARN] Adresse de guide Non NULL!")
            guide_adresse = input("Adresse de guide : ")

        guide_date_embauche = input("Date embauché de guide (DD/MM/YYYY): ")
        while(validate_date_2(guide_date_embauche) == False):
            print("[WARN] Le format de Date embauché de guide n'est pas bon!")
            guide_date_embauche = input("Date embauché de guide (DD/MM/YYYY): ")

        guide = {
            "nom": guide_nom,
            "prenom": guide_prenom,
            "adresse": guide_adresse,
            "date_embauche": guide_date_embauche,
        }
        guides.append(guide)
        count += 1
    guides_json = json.dumps(guides)
    try:
        cur.execute("INSERT INTO Exposition VALUES ('%s')" % nom)
        cur.execute(
            "INSERT INTO ExpositionTemporaire VALUES ('%s', TO_DATE('%s', 'DD/MM/YYYY'), TO_DATE('%s', 'DD/MM/YYYY'), '%s', '%s', '%s')" %
            (nom, date_debut, date_fin, salles_json, panneaux_json, guides_json)
        )
        connexion.commit()
        print("[INFO] L'exposition temporaire a été créé avec succès.")
    except psycopg2.Error as e:
        print("[ERREUR] Une erreur s'est produite lors de la création du exposition :", e)



# creation de l'exposition permanente
def  create_exposition_permanente(cur, connexion):
    print("---------creation d'un exposition permanente----------")
    # nom du exposition permanent
    nom = ""
    while len(nom) == 0:
        nom = input("Nom du exposition permanente: ")
        cur.execute("SELECT * FROM Exposition WHERE nom = '%s'" % nom)
        row = cur.fetchone()
        # Verification l'exposition deja existe
        if row:
            print("[ERREUR] L'exposition permanente existe déjà dans la base de données.")
            return
    # guide
    nb_guide = input("Combien de guide qui est responsable de cette exposition: ")
    while not nb_guide or int(nb_guide) < 0:
        print("[WARN] Nombre de guide doit > 0 et Non NULL")
        nb_guide = input("Combien de guide qui est responsable de cette exposition: ")
    count = 0
    guides = []
    while (count<int(nb_guide)):
        guide_nom = input("Nom de guide : ")
        while len(guide_nom) == 0:
            print("[WARN] Nom de guide Non NULL!")
            guide_nom = input("Nom de guide : ")
        
        guide_prenom = input("Prénom de guide : ")
        while len(guide_prenom) == 0:
            print("[WARN] Prénom de guide Non NULL!")
            guide_prenom = input("Prénom de guide : ")

        guide_adresse = input("Adresse de guide : ")
        while len(guide_adresse) == 0:
            print("[WARN] Adresse de guide Non NULL!")
            guide_adresse = input("Adresse de guide : ")

        guide_date_embauche = input("Date embauché de guide : ")
        while len(guide_date_embauche) == 0:
            print("[WARN] Date embauché de guide Non NULL!")
            guide_date_embauche = input("Date embauché de guide : ")

        # creneau
        nb_creneau = input("Nombre de créneau: ")
        while(not nb_creneau or int(nb_creneau) <= 0):
            print("[WARN] Nombre de créneau doit > 0")
            nb_creneau = input("Combien de créneau de guide : ")
        creneaux = []
        nb_creneau = int(nb_creneau)
        while nb_creneau > 0:
            jour = input("Jour (lundi-dimanche) : ")
            while not jour or jour not in ['lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi','dimanche']:
                print("[WARN] Jour Non NULL et doit etre 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi','dimanche'")
                jour = input("Jour (lundi-dimanche) : ")
            while 1:
                heure_debut = input("Heure début (HH:MM:SS) : ")
                if not validate_time_format(heure_debut):
                    print("[WARN] Le format de l'heure début n'est pas bon")
                    heure_debut = input("Heure début (HH:MM:SS) : ")
                heure_fin = input("Heure fin (HH:MM:SS) : ")
                if not validate_time_format(heure_debut):
                    print("[WARN] Le format de l'heure fin n'est pas bon")
                    heure_fin = input("Heure fin (HH:MM:SS) : ")
                # verifier le format de heure debut et fin, heure_fin > heure_debut
                # datetime
                heure_debut_dt = datetime.strptime(heure_debut, "%H:%M:%S")
                heure_fin_dt = datetime.strptime(heure_fin, "%H:%M:%S")
                if heure_debut_dt < heure_fin_dt:
                    break
                else:
                    print("[WARN] L'heure fin doit être supérieure à l'heure début")

            creneau = {
                "jour": jour,
                "heure_debut": heure_debut,
                "heure_fin": heure_fin,
            }
            creneaux.append(creneau)
            nb_creneau -= 1
        
        guide = {
            "nom": guide_nom,
            "prenom": guide_prenom,
            "adresse": guide_adresse,
            "date_embauche": guide_date_embauche,
            "creneau": creneaux,
        }
        guides.append(guide)
        count += 1    
    guides_json = json.dumps(guides)
    try:
        cur.execute("INSERT INTO Exposition VALUES ('%s')" % nom)
        if len(guides) != 0:
            cur.execute(
                "INSERT INTO ExpositionPermanente VALUES ('%s', '%s')" %
                (nom, guides_json)
            )
        else:
            cur.execute(
                "INSERT INTO ExpositionPermanente VALUES ('%s')" %
                (nom)
            )
        connexion.commit()
        print("[INFO] L'exposition permanente a été créé avec succès.")
    except psycopg2.Error as e:
        print("[ERREUR] Une erreur s'est produite lors de la création du exposition :", e)
    

# creation de l'oeuvre
def create_oeuvre(cur, connexion) :
    print("---------creation d'un oeuvre----------")
    # titre de l'oeuvre
    titre = ""
    while len(titre) == 0:
        titre = input("Titre de l'oeuvre: ")
        cur.execute("SELECT * FROM Oeuvre WHERE titre = '%s'" % titre)
        row = cur.fetchone()
        # Verification l'exposition deja existe
        if row:
            print("[ERREUR] L'oeuvre existe déjà dans la base de données.")
            return    
    # type de l'oeuvre
    typ = input("Type de l'oeuvre (peinture, sculpture, photo) : ")    # verification type de l'oeuvre
    # verification type de l'oeuvre
    while typ not in ['peinture','sculpture','photo']:
        print("[WARN] Le type de l'oeuvre n'est pas bon!")
        typ = input("Type de l'oeuvre (peinture, sculpture, photo) : ")
    # date
    date = input("Date (DD/MM/YYYY): ")
    while (validate_date_2(date) == False) :
        print("[WARN] Le format de date ne suffit pas!")
        date = input("Date (DD/MM/YYYY): ")
    # dimension: length, width
    # length de l'oeuvre
    length = input("Length de l'oeuvre: ")
    # length > 0
    while not length or int(length) <= 0 :
        print("[WARN] Le length de l'oeuvre doit > 0 et Non Null!")
        length = input("Length de l'oeuvre: ")
    # width de l'oeuvre
    width = input("Width de l'oeuvre: ")
    dimension = {
        "length": length,
        "width": width,
    }
    dimension = json.dumps(dimension)
    # width > 0
    while not width or int(width) <= 0 :
        print("[WARN] Le width de l'oeuvre doit > 0 et Non Null!")
        width = input("Width de l'oeuvre: ")
    # prix_acquisition
    # verification de prix d'acquisition
    prix_acquisition = input("Prix d'acquisition de l'oeuvre: ")
    while 1:
        try:
            prix_acquisition = int(prix_acquisition)
            if prix_acquisition > 0:
                break
            else:
                print("[WARN] Le prix d'acquisition de l'oeuvre doit >= 0!")
                prix_acquisition = input("Prix d'acquisition de l'oeuvre: ")
        except ValueError:
            print("[WARN] Entrez un nombre!")
            prix_acquisition = input("Prix d'acquisition de l'oeuvre: ")
    # restauree
    restauree = input("Restaurée? (y pour yes) : ")
    if restauree == "y":
        restauree = True
    else:
        restauree = False 
    # l'auteur de l'oeuvre
    verif=0
    while(verif==0) :
        lecture_auteur(cur,connexion)
        auteur_nom = input("Le nom de l'auteur : ")
        sql = "SELECT * FROM Auteur WHERE nom='%s'" % auteur_nom
        cur.execute(sql)
        row = cur.fetchone()
        if (row) :
            verif = 1
            # obtenir auteur_id
            aut = row[0]
    # l'exposition au quel il partient
    verif = 0
    while(verif==0) :
        # afficher la liste d'exposition
        lecture_exposition(cur,connexion)
        # affiche_expo()
        exposition = input("Le nom d'exposition : ")
        sql = "SELECT * FROM Exposition WHERE nom='%s'" % exposition
        cur.execute(sql)
        row = cur.fetchone()
        if (row) :
            verif = 1
    verif=0
    while(verif==0) :
        # afficher la liste de musée
        lecture_musee(cur,connexion)
        musee = input("Le nom de musée: ")
        sql = "SELECT * FROM Musee WHERE nom='%s'" % musee
        cur.execute(sql)
        row = cur.fetchone()
        if (row) :
            verif = 1   
    # ajouter l'oeuvre
    try:
        cur.execute("INSERT INTO Oeuvre VALUES ('%s','%s',TO_DATE('%s', 'DD/MM/YYYY'), '%s', '%d', '%s', '%d', '%s', '%s')" %
        (titre, typ, date, dimension, prix_acquisition, restauree, aut, exposition, musee)
        )
        connexion.commit()
        print("[INFO] L'oeuvre a été créé avec succès.")
    except psycopg2.Error as e:
        print("[ERREUR] Une erreur s'est produite lors de la création de l'oeuvre :", e)

# creation de l'emprunte
def create_emprunte(cur, connexion):
    print("---------creation de l'emprunte----------")
    # musee
    verif=0
    while(verif==0) :
        # afficher la liste de musée
        lecture_musee(cur,connexion)
        musee = input("Le nom de musée: ")
        sql = "SELECT * FROM Musee WHERE nom='%s'" % musee
        cur.execute(sql)
        row = cur.fetchone()
        if (row) :
            verif = 1
    verif = 0
    # oeuvre
    while(verif==0) :
        # afficher la liste de exposition
        lecture_exposition(cur,connexion)
        oeuvre = input("Le titre de l'oeuvre: ")
        sql = "SELECT * FROM Oeuvre WHERE titre='%s'" % oeuvre
        cur.execute(sql)
        row = cur.fetchone()
        if (row) :
            verif = 1
    # date_debut
    date_debut = input("Date début de l'emprunte (DD/MM/YYYY): ")
    # verification format de date
    while (validate_date_2(date_debut) == False) :
        print("[WARN] Le format de date début ne suffit pas!")
        date_debut = input("Date début de l'emprunte (DD/MM/YYYY): ")
    # date_fin
    date_fin = input("Date fin de l'emprunte (DD/MM/YYYY): ")
    # verification format de date
    while (validate_date_2(date_fin) == False) :
        print("[WARN] Le format de date fin ne suffit pas!")
        date_fin = input("Date fin de l'emprunte (DD/MM/YYYY): ")
    try:
        cur.execute("INSERT INTO Emprunte VALUES ('%s','%s',TO_DATE('%s', 'DD/MM/YYYY'), TO_DATE('%s', 'DD/MM/YYYY'))" %
        (musee, oeuvre, date_debut, date_fin)
        )
        connexion.commit()
        print("[INFO] L'emprunte a bien été ajouté")
    except psycopg2.DataError as e:
        print("[ERREUR] Une erreur s'est produite lors de la création de l'emprunte :", e)

# creation de compte
def create_compte(cur, connexion):
    id_compte = input("Nom du compte: ")
    # id NOT NULL
    while id_compte == '':
        print("\n[WARN] nom du compte non null!")
        id_compte = input("Nom du compte: ")
    mdp = input("Mot de passe: ")
    # mdp NOT NULL
    while mdp == '':
        print("\n[WARN] mot de passe non null!")
        mdp = input("Mot de passe: ")
    # verification de role
    role = input("Rôle du compte (admin, utilisateur): ")
    if role not in ['admin', 'utilisateur']:
        print("\n[WARN] Le role soit admin, soit utilisateur!")
    try:
        sql = "INSERT INTO Comptes VALUES ('%s', '%s','%s');" % (id_compte,mdp,role)
        cur.execute(sql)
        connexion.commit()
        print("[INFO] Le compte a bien été ajouté")
    except psycopg2.DataError as e:
        print("\n[ERREUR] Une erreur s'est produite lors de la création de compte :", e)









