import json
import psycopg2

# # test
# connexion = psycopg2.connect("dbname ='dbnf18p049' user='nf18p049' password='sbX7OFf3sl77' host='tuxa.sme.utc'")
# cur = connexion.cursor()

def lecture_auteur(cur, connexion):
    cur.execute("SELECT * FROM Auteur")
    rows = cur.fetchall()
    print("---------------------------")
    for row in rows:
        id, nom, prenom, date_naissance, date_mort = row
        print(f"ID: {id}")
        print(f"Nom: {nom}")
        print(f"Prénom: {prenom}")
        print(f"Date de naissance: {date_naissance}")
        print(f"Date de mort: {date_mort}")
        print("----------------------------------")

def lecture_musee(cur, connexion):
    cur.execute("SELECT * FROM Musee")
    print("---------------------------")
    result = cur.fetchall()
    for row in result:
        print("Nom:", row[0])
        print("Adresse:", row[1])
        print("---------------------------")

def lecture_exposition(cur, connexion):
    cur.execute("SELECT * FROM Exposition")
    result = cur.fetchall()

    for row in result:
        print("Nom: ", row[0])


def lecture_exposition_permanente(cur, connexion):
    try:
        cur.execute("SELECT * FROM ExpositionPermanente")
        result = cur.fetchall()
        for row in result:
            print("---------------------------")
            nom = row[0]
            guide = row[1]
            print("Nom: ", nom)
            print("Guide:")
            if guide:
                for guide_info in guide:
                    print("  Nom: ", guide_info['nom'])
                    print("  Prénom: ", guide_info['prenom'])
                    print("  Adresse: ", guide_info['adresse'])
                    print("  Date d'embauche: ", guide_info['date_embauche'])
                    print("  Créneaux:")
                    for creneau in guide_info['creneau']:
                        print("    Jour: ", creneau['jour'])
                        print("    Heure de début: ", creneau['heure_debut'])
                        print("    Heure de fin: ", creneau['heure_fin'])
                    print("---------------------------")

    except (Exception, psycopg2.DatabaseError) as error:
        print("Erreur lors de la lecture de l'exposition permanente:", error)

def lecture_exposition_temporaire(cur, connexion):
    cur.execute("SELECT * FROM ExpositionTemporaire")
    result = cur.fetchall()
    print("----------------------")
    for row in result:
        nom = row[0]
        date_debut = row[1]
        date_fin = row[2]
        salle = row[3]
        panneau_explicatif = row[4]
        guide = row[5]

        print("Nom:", nom)
        print("Date de début:", date_debut)
        print("Date de fin:", date_fin)
        print("Salle:", salle)
        print("Panneau explicatif:", panneau_explicatif)
        print("Guide:", guide)
        print("----------------------")


def lecture_oeuvre(cur, connexion):
    cur.execute("SELECT * FROM Oeuvre")
    result = cur.fetchall()
    print("------------------------------")
    for row in result:
        titre = row[0]
        type_oeuvre = row[1]
        date = row[2]
        dimension = row[3]
        prix_acquisition = row[4]
        restauree = row[5]
        auteur = row[6]
        exposition = row[7]
        musee = row[8]

        print("Titre:", titre)
        print("Type d'oeuvre:", type_oeuvre)
        print("Date:", date)
        print("Dimension:", dimension)
        print("Prix d'acquisition:", prix_acquisition)
        print("Restaurée:", restauree)
        print("Auteur:", auteur)
        print("Exposition:", exposition)
        print("Musée:", musee)
        print("---------------------")

def lecture_emprunte(cur, connexion):
    cur.execute("SELECT * FROM Emprunte")
    rows = cur.fetchall()
    print("------------------------------")
    for row in rows:
        musee = row[0]
        oeuvre = row[1]
        date_debut = row[2]
        date_fin = row[3]
        print("Musee:", musee)
        print("Oeuvre:", oeuvre)
        print("Date de debut:", date_debut)
        print("Date de fin:", date_fin)
        print("------------------------------")


