DROP VIEW IF EXISTS v_OeuvreMuseeAuteur;
DROP VIEW IF EXISTS v_ExpositionPermanenteGuideCreneau;
DROP VIEW IF EXISTS v_ExpositionTenporaireGuideCreneau;
DROP VIEW IF EXISTS v_EmprunteMusee;

CREATE VIEW v_OeuvreMuseeAuteur AS
  SELECT titre, type, exposition, m.nom as musee,
  a.nom as nomAuteur, a.prenom as prenomAuteur, a.date_naissance, a.date_mort
  FROM Oeuvre o
  JOIN Musee m ON o.musee = m.nom
  JOIN Auteur a ON o.auteur = a.id;

CREATE VIEW v_ExpositionPermanenteGuideCreneau AS
  SELECT ep.nom as expo, g->>'nom' AS nomGuide, g->>'prenom' AS prenomGuide,
  c->>'jour' AS Jour, c->>'heure_debut' AS HeureDebut, c->>'heure_fin' AS HeureFin
  FROM ExpositionPermanente ep, JSON_ARRAY_ELEMENTS(ep.guide) g, JSON_ARRAY_ELEMENTS(CAST(g->>'creneau' AS JSON)) c;

CREATE VIEW v_ExpositionTenporaireGuideCreneau AS
  SELECT et.nom as expo, g->>'nom' AS nomGuide, g->>'prenom' AS prenomGuide
  FROM ExpositionTemporaire et, JSON_ARRAY_ELEMENTS(et.guide) g;

CREATE VIEW v_EmprunteMusee AS
  SELECT m.nom, e.oeuvre, e.date_debut, e.date_fin
  FROM Emprunte e JOIN Musee m ON e.musee = m.nom;

SELECT * FROM v_OeuvreMuseeAuteur;
SELECT * FROM Oeuvre;

SELECT titre, type, CAST(dimension->>'length' AS INTEGER) AS longueur,CAST(dimension->>'width' AS INTEGER) AS largeur,exposition
FROM Oeuvre o;

SELECT titre FROM v_OeuvreMuseeAuteur WHERE nomAuteur = 'Van Gogh';

SELECT type, count(*) AS Nombre_Oeuvres
FROM v_OeuvreMuseeAuteur
WHERE musee = 'Le Louvre'
GROUP BY type;

SELECT nomAuteur, prenomAuteur, count(*) AS Nombre_Oeuvres
FROM v_OeuvreMuseeAuteur
WHERE date_mort IS NULL AND musee = 'Le Louvre'
GROUP BY prenomAuteur, nomAuteur
ORDER BY nomAuteur;

SELECT titre
FROM v_OeuvreMuseeAuteur o, v_EmprunteMusee e
WHERE musee ='Le Louvre' AND e.oeuvre = o.titre;

SELECT titre, nomAuteur, prenomAuteur
FROM v_OeuvreMuseeAuteur o LEFT JOIN v_EmprunteMusee e ON o.titre = e.oeuvre
WHERE musee != 'Le Louvre' AND date_debut IS NULL;

SELECT titre AS ouevre,  nomAuteur, musee, exposition AS expTemporaire, (date_fin -date_debut) AS duree
FROM v_OeuvreMuseeAuteur o JOIN ExpositionTemporaire et ON o.exposition = et.nom
WHERE musee != 'Le Louvre' AND (date_fin - date_debut) > 20;

SELECT DISTINCT nomGuide, prenomGuide
FROM v_ExpositionPermanenteGuideCreneau
UNION
SELECT DISTINCT nomGuide, prenomGuide
FROM v_ExpositionTenporaireGuideCreneau;

SELECT * FROM v_ExpositionPermanenteGuideCreneau ep
LEFT JOIN v_ExpositionTenporaireGuideCreneau et
ON ep.nomGuide = et.nomGuide
WHERE et.nomGuide IS NULL;
/**/
SELECT Jour, HeureDebut, HeureFin
FROM v_ExpositionPermanenteGuideCreneau
WHERE nomGuide ='Dupont'
GROUP BY Jour, HeureDebut, HeureFin;
/**/
SELECT nomGuide, prenomGuide, HeureDebut, HeureFin
FROM v_ExpositionPermanenteGuideCreneau
WHERE HeureDebut < '11:00:00' AND HeureFin > '11:00:00';
/**/
SELECT nomAuteur, prenomAuteur, titre
FROM v_OeuvreMuseeAuteur o
LEFT JOIN ExpositionPermanente ep ON ep.nom = o.exposition
WHERE ep.nom IS NOT NULL
INTERSECT
SELECT nomAuteur, prenomAuteur, titre
FROM v_OeuvreMuseeAuteur o
LEFT JOIN ExpositionTemporaire et ON et.nom = o.exposition
WHERE et.nom IS NOT NULL;

SELECT nom as expoTemporaire, count(*) AS Nb_Salles, SUM(CAST(s->>'capacite' AS INTEGER)) AS Capacite
FROM ExpositionTemporaire e, JSON_ARRAY_ELEMENTS(e.salle) s
GROUP BY nom;

SELECT CAST(s->>'num' AS INTEGER) AS numeroSalle, CAST(s->>'capacite' AS INTEGER) AS capaciteSalle,
pe->>'texte' AS description
FROM ExpositionTemporaire et, JSON_ARRAY_ELEMENTS(et.salle) s, JSON_ARRAY_ELEMENTS(et.panneauExplicatif) pe
WHERE nom = 'Les Choses. Une histoire de la nature morte';
