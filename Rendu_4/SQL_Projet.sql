

DROP TABLE IF EXISTS Emprunte;
DROP TABLE IF EXISTS PanneauExplicatif;
DROP TABLE IF EXISTS Salle;
DROP TABLE IF EXISTS Creneau;
DROP TABLE IF EXISTS Guide;
DROP TABLE IF EXISTS Oeuvre;
DROP TABLE IF EXISTS Musee;
DROP TABLE IF EXISTS ExpositionPermanente;
DROP TABLE IF EXISTS ExpositionTemporaire;
DROP TABLE IF EXISTS Exposition;
DROP TABLE IF EXISTS Auteur;
DROP TABLE IF EXISTS Comptes;
DROP TYPE IF EXISTS nom_jour;
DROP TYPE IF EXISTS role_compte;
DROP TYPE IF EXISTS type_oeuvre;

CREATE TABLE Auteur(
	id INTEGER PRIMARY KEY,
	nom VARCHAR NOT NULL,
	prenom VARCHAR NOT NULL,
	date_naissance DATE,
	date_mort DATE,
	CHECK(date_mort>date_naissance)
);

CREATE TABLE Exposition(
	nom VARCHAR PRIMARY KEY
);

CREATE TABLE ExpositionTemporaire(
	nom VARCHAR PRIMARY KEY,
	date_debut DATE NOT NULL,
	date_fin DATE NOT NULL,
	FOREIGN KEY (nom) REFERENCES Exposition(nom),
	CHECK(date_fin>date_debut)
);

CREATE TABLE ExpositionPermanente(
	nom VARCHAR PRIMARY KEY,
	FOREIGN KEY (nom) REFERENCES Exposition(nom)
);

CREATE TABLE Musee(
	nom VARCHAR PRIMARY KEY,
	adresse VARCHAR NOT NULL
);

CREATE TYPE type_oeuvre AS ENUM ('peinture','sculpture','photo');
CREATE TABLE Oeuvre (
    titre VARCHAR PRIMARY KEY,
    type type_oeuvre,
    date DATE,
    dimension VARCHAR,
    prix_acquisition INTEGER,
    restauree BOOLEAN,
    auteur INTEGER NOT NULL,
    exposition VARCHAR NOT NULL,
    musee VARCHAR NOT NULL,
    FOREIGN KEY (auteur) REFERENCES Auteur(id),
    FOREIGN KEY (exposition) REFERENCES Exposition(nom),
    FOREIGN KEY (musee) REFERENCES Musee(nom),
    CHECK(prix_acquisition > 0)
);

CREATE TABLE Emprunte(
	musee VARCHAR,
	oeuvre VARCHAR,
	date_debut DATE,
	date_fin DATE,
	FOREIGN KEY(musee) REFERENCES Musee(nom),
	FOREIGN KEY(oeuvre) REFERENCES Oeuvre(titre),
PRIMARY KEY(musee,oeuvre)
);

CREATE TABLE Guide(
  	id INTEGER PRIMARY KEY,
  	nom VARCHAR NOT NULL,
  	prenom VARCHAR NOT NULL,
  	adresse VARCHAR NOT NULL,
  	date_embauche DATE NOT NULL,
	exposition_temporaire VARCHAR
);

CREATE TYPE nom_jour AS ENUM('Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche');
CREATE TABLE Creneau(
  	guide INTEGER,
  	expo VARCHAR,
  	jour nom_jour,
  	heure_debut TIME  NOT NULL,
  	heure_fin TIME NOT NULL,
CHECK (heure_fin > heure_debut AND EXTRACT(EPOCH FROM heure_fin - heure_debut) = 7200),
  	FOREIGN KEY(guide) REFERENCES Guide(id),
  	FOREIGN KEY(expo) REFERENCES ExpositionPermanente(nom),
  	PRIMARY KEY (guide, jour, heure_debut)
);

CREATE TABLE Salle(
  	num INTEGER PRIMARY KEY,
  	capacite INTEGER NOT NULL,
  	exposition_temporaire VARCHAR NOT NULL,
  	CHECK(capacite > 0),
  	FOREIGN KEY(exposition_temporaire) REFERENCES ExpositionTemporaire(nom)
);

CREATE TABLE PanneauExplicatif(
num INTEGER PRIMARY KEY,
  	texte VARCHAR NOT NULL,
  	exposition_temporaire VARCHAR NOT NULL,
  	salle INTEGER NOT NULL,
FOREIGN KEY(exposition_temporaire) REFERENCES ExpositionTemporaire(nom),
  	FOREIGN KEY(salle) REFERENCES Salle(num)
);

CREATE TYPE role_compte AS ENUM('utilisateur','admin');
CREATE TABLE Comptes(
	id VARCHAR PRIMARY KEY,
	mdp VARCHAR NOT NULL,
	role role_compte NOT NULL
);


DROP VIEW IF EXISTS v_Exposition;

CREATE VIEW v_Exposition AS
SELECT et.nom
FROM ExpositionTemporaire et
UNION
SELECT ep.nom
FROM ExpositionPermanente ep
Except
SELECT nom
FROM Exposition;
-- Permet de verifier: Projection(Exposition, nom) = Projection(ExpositionTemporaire, nom) UNION Projection(ExpositionPermanente, nom)
-- en donnant la liste des expositions ne respectant pas la contrainte (si la base de données est remplies correctement, la vue n'affiche rien)

DROP VIEW IF EXISTS v_Oeuvre_Expo;

CREATE VIEW v_Oeuvre_Expo AS
SELECT exposition
FROM Oeuvre
Except
SELECT nom
FROM Exposition;
-- Permet de verifier: Projection(Oeuvre, exposition) = Projection(Exposition,nom)
-- en donnant la liste des exposition ne respectant pas la contrainte (si la base de données est remplies correctement, la vue n'affiche rien)

DROP VIEW IF EXISTS v_ExpoTemp_ExpoPerma;

CREATE VIEW v_ExpoTemp_ExpoPerma AS
SELECT et.nom
FROM ExpositionTemporaire et
Intersect
SELECT ep.nom
FROM ExpositionPermanente ep;
-- Permet de verifier: Intersection(Projection(Œuvre, musee), Projection(Emprunte, musee)) = {}
-- en donnant la liste des expositions ne respectant pas la contrainte (si la base de données est remplies correctement, la vue n'affiche rien)

DROP VIEW IF EXISTS v_ExpoTemp_Guide;
CREATE VIEW v_ExpoTemp_Guide AS
SELECT nom
FROM ExpositionTemporaire
Except
SELECT exposition_temporaire
FROM Guide;
-- Permet de verifier: Projection(Exposition_temporaire, guide) = Projection(Guide,exposition_temporaire)
-- en donnant la liste des salles ne respectant pas la contrainte (si la base de données est remplies correctement, la vue n'affiche rien)

DROP VIEW IF EXISTS v_ExpoTemp_Salle;

CREATE VIEW v_ExpoTemp_Salle AS
SELECT exposition_temporaire
FROM Salle
Except
SELECT nom
FROM ExpositionTemporaire;
-- Permet de verifier: Projection(Salle, exposition) = Projection(ExpositionTemporaire, nom)
-- en donnant la liste des exposition temporaires ne respectant pas la contrainte (si la base de données est remplies correctement, la vue n'affiche rien)

DROP VIEW IF EXISTS v_Salle_Panneau;
CREATE VIEW v_Salle_Panneau AS
SELECT num
FROM Salle
Except
SELECT salle
FROM PanneauExplicatif;
-- Permet de verifier: Projection(Salle, numero) = Projection(PanneauExplicatif,salle)
-- en donnant la liste des salles ne respectant pas la contrainte (si la base de données est remplies correctement, la vue n'affiche rien)


DROP VIEW IF EXISTS v_Oeuvre_Musee;
CREATE VIEW v_Oeuvre_Musee AS
SELECT musee
FROM Oeuvre
Except
SELECT nom
FROM Musee;
-- Permet de verifier: Projection(Oeuvre, musee) = Projection(Musee,nom)
-- en donnant la liste des musee ne respectant pas la contrainte (si la base de données est remplies correctement, la vue n'affiche rien)

INSERT INTO Auteur VALUES(1,'Van Gogh','Vincent',TO_DATE('30/03/1853', 'DD/MM/YYYY'),TO_DATE('29/07/1890', 'DD/MM/YYYY'));

INSERT INTO Auteur VALUES(2 ,'Monet','Claude',  TO_DATE('14/11/1840', 'DD/MM/YYYY'),TO_DATE('19/12/1926', 'DD/MM/YYYY'));

INSERT INTO Auteur VALUES(3 ,'Michelangelo','Buonarroti',  TO_DATE('06/03/1475', 'DD/MM/YYYY'),TO_DATE('18/02/1564', 'DD/MM/YYYY'));

INSERT INTO Auteur VALUES(4 ,'Banksy',' ',  TO_DATE('28/07/1974', 'DD/MM/YYYY'),NULL);

INSERT INTO Auteur VALUES(5 ,'Belin','Valérie',  TO_DATE('03/02/1964', 'DD/MM/YYYY'),NULL);

INSERT INTO Auteur VALUES(6 ,'Rodin','Auguste',  TO_DATE('12/11/1840', 'DD/MM/YYYY'),TO_DATE('17/11/1917', 'DD/MM/YYYY'));

INSERT INTO Exposition VALUES ('Impressionnisme');

INSERT INTO Exposition VALUES ('Les images intranquilles');

INSERT INTO Exposition VALUES ('Noir & Blanc');

INSERT INTO Exposition VALUES ('Les Choses. Une histoire de la nature morte');

INSERT INTO Exposition VALUES ('Le Corps et l Ame');

INSERT INTO ExpositionPermanente VALUES ('Impressionnisme');

INSERT INTO ExpositionPermanente VALUES ('Noir & Blanc');

INSERT INTO ExpositionPermanente VALUES ('Le Corps et l Ame');

INSERT INTO ExpositionTemporaire VALUES ('Les images intranquilles',TO_DATE('03/04/2015', 'DD/MM/YYYY'),TO_DATE('03/05/2015', 'DD/MM/YYYY'));

INSERT INTO ExpositionTemporaire VALUES ('Les Choses. Une histoire de la nature morte',TO_DATE('12/10/2022', 'DD/MM/YYYY'),TO_DATE('23/01/2023', 'DD/MM/YYYY'));

INSERT INTO Musee VALUES ('Le Louvre', 'Rue de Rivoli 75001 Paris');

INSERT INTO Musee VALUES ('Centre Pompidou', 'Place Georges-Pompidou 75004 Paris');

INSERT INTO Oeuvre VALUES('Impression, soleil levant', 'peinture', '01-04-1874'::date, '48 x 63', NULL, 'TRUE', 2, 'Impressionnisme', 'Le Louvre');

INSERT INTO Oeuvre VALUES('Les Coquelicots', 'peinture', '01-04-1873'::date, '50x 65', NULL, 'TRUE', 2, 'Impressionnisme', 'Le Louvre');

INSERT INTO Oeuvre VALUES('La nuit étoilé', 'peinture', '01/06/1889'::date, '74 x 92', NULL, 'TRUE', 1, 'Impressionnisme' , 'Le Louvre');

INSERT INTO Oeuvre VALUES('Champ de blé aux corbeaux', 'peinture', TO_DATE('01/07/1890', 'DD/MM/YYYY'), '50,5X100,5', NULL, 'TRUE', 1, 'Impressionnisme' , 'Le Louvre');

INSERT INTO Oeuvre VALUES('Les tournesols', 'peinture', TO_DATE('01/08/1888', 'DD/MM/YYYY'), '92X73', NULL, 'TRUE', 1, 'Impressionnisme' , 'Le Louvre');

INSERT INTO Oeuvre VALUES('David', 'sculpture', '01/01/1504'::date, '517 x 199 ', NULL, 'TRUE', 3,'Le Corps et l Ame' , 'Le Louvre');

INSERT INTO Oeuvre VALUES('L’esclave Rebelle', 'sculpture', '01/01/1516'::date, 'H : 209 ', NULL, 'TRUE', 3,'Le Corps et l Ame' , 'Le Louvre');

INSERT INTO Oeuvre VALUES('La petite fille au ballon', 'peinture', '01/01/2002'::date, '... × … ', NULL, 'TRUE', 4, 'Impressionnisme' , 'Le Louvre');

INSERT INTO Oeuvre VALUES('Bodybuilders', 'photo', '01-01-1999'::date, '160 x 125', NULL, 'TRUE', 5, 'Les images intranquilles', 'Centre Pompidou');

INSERT INTO Oeuvre VALUES('Still Life with Dish', 'photo', '05-06-2014'::date, '5 x 8', 1000, 'FALSE', 5, 'Les Choses. Une histoire de la nature morte', 'Le Louvre');

INSERT INTO Oeuvre VALUES('Mannequins', 'photo', '03-04-2003'::date, '155 x 125', 1500, 'FALSE', 5,'Noir & Blanc', 'Le Louvre');

INSERT INTO Oeuvre VALUES('Baiser', 'sculpture', '01-01-1900'::date, '182X112X117', 1500, 'FALSE', 6,'Le Corps et l Ame', 'Le Louvre');

INSERT INTO Guide VALUES (1, 'Dupont', 'Jean', 'rue Mouchotte Compiègne', '2019-06-01'::date, NULL);

INSERT INTO Guide VALUES (2, 'Schmidt', 'Valerie', 'rue Gambetta Paris', '2019-06-01'::date, NULL);

INSERT INTO Guide VALUES (3, 'Martin', 'Marie', 'rue d’Argentine Paris', '2020-02-02'::date, 'Les images intranquilles');

INSERT INTO Creneau VALUES (1, 'Impressionnisme', 'Mardi', '10:00:00', '12:00:00');

INSERT INTO Creneau VALUES (2, 'Impressionnisme', 'Mercredi', '14:00:00', '16:00:00');

INSERT INTO Creneau VALUES (2, 'Noir & Blanc', 'Lundi', '13:00:00', '15:00:00');

INSERT INTO Creneau VALUES (3, 'Noir & Blanc', 'Lundi', '11:00:00', '13:00:00');

INSERT INTO Emprunte VALUES ('Le Louvre', 'Bodybuilders', TO_DATE('12/12/2022', 'DD/MM/YYYY'), TO_DATE('10/06/2023', 'DD/MM/YYYY'));
INSERT INTO Emprunte VALUES ('Centre Pompidou', 'Mannequins', TO_DATE('12/12/2022', 'DD/MM/YYYY'), TO_DATE('10/06/2023', 'DD/MM/YYYY'));
INSERT INTO Salle VALUES (1, 350, 'Les images intranquilles');
INSERT INTO Salle VALUES (2, 450, 'Les Choses. Une histoire de la nature morte');
INSERT INTO Salle VALUES (3, 300, 'Les Choses. Une histoire de la nature morte');
INSERT INTO PanneauExplicatif VALUES (1, 'Description', 'Les Choses. Une histoire de la nature morte', 2);
INSERT INTO PanneauExplicatif VALUES (2, 'Histoire', 'Les Choses. Une histoire de la nature morte', 2);
INSERT INTO PanneauExplicatif VALUES (3, 'Description', 'Les Choses. Une histoire de la nature morte', 3);

INSERT INTO Comptes VALUES('admin','123','admin');
INSERT INTO Comptes VALUES('utilisateur','123','utilisateur');


SELECT titre, type, dimension, musee FROM Oeuvre;

SELECT * FROM Guide;

SELECT titre FROM Oeuvre
	JOIN Auteur ON Auteur.id=Oeuvre.auteur
	WHERE Auteur.nom = 'Van Gogh';

SELECT type, count(*) AS Nombre_Oeuvres
FROM Oeuvre
WHERE musee = 'Le Louvre'
GROUP BY type;

SELECT Auteur.nom, Auteur.prenom, count(*) AS Nombre_Oeuvres
FROM Auteur JOIN Oeuvre ON Auteur.id = Oeuvre.auteur
WHERE Auteur.date_mort IS NULL AND Oeuvre.musee = 'Le Louvre'
GROUP BY Auteur.id
ORDER BY Auteur.nom;

SELECT Guide.nom, Guide.prenom
	FROM Guide JOIN Creneau ON Guide.id = Creneau.guide
	WHERE Guide.expo_temporaire IS NOT NULL;

SELECT c.jour, c.heure_debut, c.heure_fin  FROM Creneau c
JOIN Guide g ON g.id = c.guide
WHERE g.nom ='Dupont'
GROUP BY c.jour, c.heure_debut, c.heure_fin;


SELECT titre,Oeuvre.musee FROM Oeuvre
JOIN Emprunte ON Emprunte.oeuvre=Oeuvre.titre
WHERE Emprunte.musee='Le Louvre';


SELECT ExpositionTemporaire.nom,count(*) as nbSalle from ExpositionTemporaire
JOIN Salle ON Salle.exposition_temporaire=ExpositionTemporaire.nom
GROUP BY ExpositionTemporaire.nom;
