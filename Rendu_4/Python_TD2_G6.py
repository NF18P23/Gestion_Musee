import psycopg2
from datetime import datetime

connexion = psycopg2.connect("dbname ='dbnf18p035' user='nf18p035' password='iAs288hxsjUR' host='tuxa.sme.utc'")
cur = connexion.cursor()
cur1 = connexion.cursor()

# ----------------------------Test contraintes-----------------------------
# Verifier si le bdd satisfait tous les contraints complexes
def check_bdd():
    status = True

    # VIEWS vérifier les contraintes sur les projections

    sql = "SELECT * FROM v_Exposition;"
    cur.execute(sql)
    rows = cur.fetchall()
    for row in rows:
        print("Erreur sur les données dans la base : l'exposition '%s' ne suffit pas le contrainte: Projection(Exposition, nom) = Projection(ExpositionTemporaire, nom) UNION Projection(ExpositionPermanente, nom)" % row)
        status = False

    sql = "SELECT * FROM v_Oeuvre_Expo;"
    cur.execute(sql)
    rows = cur.fetchall()
    for row in rows:
        print("Erreur sur les données dans la base : l'exposition '%s' n'a aucun oeuvre présenté" % row)
        status = False

    sql = "SELECT * FROM v_ExpoTemp_ExpoPerma;"
    cur.execute(sql)
    rows = cur.fetchall()
    for row in rows:
        print("Erreur sur les données dans la base : l'exposition '%s' ne suffit pas le contraint: Intersection(Projection(Œuvre, musee), Projection(Emprunte, musee)) = {}" % row)
        status = False

    sql = "SELECT * FROM v_ExpoTemp_Guide;"
    cur.execute(sql)
    rows = cur.fetchall()
    for row in rows:
        print("Erreur sur les données dans la base : l'exposition temporaire '%s' n'a auncun guide" % row)
        status = False

    sql = "SELECT * FROM v_ExpoTemp_Salle;"
    cur.execute(sql)
    rows = cur.fetchall()
    for row in rows:
        print("Erreur sur les données dans la base : l'exposition temporaire '%s' n'est relié avec aucune salle." % row)
        status = False

    sql = "SELECT * FROM v_Salle_Panneau;"
    cur.execute(sql)
    rows = cur.fetchall()
    for row in rows:
        print("Erreur sur les données dans la base : la salle '%d' n'a aucun panneau pour présenter" % row)
        status = False

    sql = "SELECT * FROM v_Oeuvre_Musee;"
    cur.execute(sql)
    rows = cur.fetchall()
    for row in rows:
        print("Erreur sur les données dans la base : le muséé '%s' n'a aucun oeuvres" % row)
        status = False

    return status


# ----------------------------Create-----------------------------
def validate(date_text):
    try:
        if date_text != datetime.strptime(date_text, "%d-%m-%Y").strftime('%d-%m-%Y'):
            raise ValueError
        return True
    except ValueError:
        return False

# creation de Auteur
def create_auteur():
   print("---------creation d'un auteur----------")
   sql = "SELECT * FROM Auteur"
   cur.execute(sql)
   row = cur.fetchone()
   while(row) :
       num = row[0] + 1
       row = cur.fetchone()
   row = 1
   while (row) :
       nom = input("Nom: ")
       cur.execute("SELECT * FROM Auteur WHERE nom = '%s'" % nom)
       row = cur.fetchone()
   prenom = input("Prenom: ")
   date_naissance =""
   while (validate(date_naissance)==False) :
       date_naissance = input("Date de naissance(DD-MM-YYYY): ")
   choix = input("Est-il mort ? (oui :o)")
   date_mort = ""
   if (choix=="o") :
       while (validate(date_mort)==False) :
           date_mort = input("Date de mort(DD-MM-YYYY): ")
   # Verification si l'auteur existe deja dans BD
   # Insertion du auteur dans la base de données
   try:
       if (date_mort):
           cur.execute(
               "INSERT INTO Auteur VALUES (%s, %s, %s, TO_DATE(%s,'DD/MM/YYYY'), TO_DATE(%s,'DD/MM/YYYY'))",
               (num, nom, prenom, date_naissance, date_mort)
           )
           connexion.commit()
       else:
           cur.execute(
               "INSERT INTO Auteur VALUES (%s, %s, %s, TO_DATE(%s,'DD/MM/YYYY'), NULL)",
               (num, nom, prenom, date_naissance)
           )
           connexion.commit()
       print("\nL'auteur a été créé avec succès.")
   except psycopg2.Error as e:
       print("\nERREUR : Une erreur s'est produite lors de la création du auteur :", e)


# creation du Exposition
def create_exposition():
   print("---------creation d'un exposition----------")
   typ = input("Exposition temporaire ou Exposition permanent? temporaire = 1 et permanent = 0: ")
   if typ == '1':
       row = 1
       while(row) :
           nom = input("Nom du exposition temporaire: ")
           cur.execute("SELECT * FROM Exposition WHERE nom = '%s'" % nom)
           row = cur.fetchone()
       date_debut=""
       while(validate(date_debut)==False) :
           date_debut = input("Date debut(DD-MM-YYYY): ")
       date_fin=""
       while(validate(date_fin)==False) :
           date_fin = input("Date fin(DD-MM-YYYY): ")


       # Verification l'exposition deja existe

       try:
           cur.execute("INSERT INTO Exposition VALUES ('%s')" % nom)
           connexion.commit()
           cur.execute(
               "INSERT INTO ExpositionTemporaire VALUES ('%s', TO_DATE('%s', 'DD-MM-YYYY'), TO_DATE('%s', 'DD-MM-YYYY'))" %
               (nom, date_debut, date_fin)
           )
           connexion.commit()
           print("\nL'exposition temporaire a été créé avec succès.")
       except psycopg2.Error as e:
           print("\nERREUR : Une erreur s'est produite lors de la création du exposition :", e)
   elif typ == '0':
       nom = input("Nom du exposition permanente: ")
       # Verification Donnees completes
       if not nom:
           print("\nERREUR : Veuillez fournir toutes les informations nécessaires pour créer l'exposition temporaire.")
           return
       # Verification l'exposition deja existe
       cur.execute("SELECT COUNT(*) FROM ExpositionPermanente WHERE nom = %s", (nom,))
       count = cur.fetchone()[0]
       if count > 0:
           print("\nERREUR : L'exposition existe déjà dans la base de données.")
           return
       # ajouter l'exposition et exposition permanente
       try:
           cur.execute("INSERT INTO Exposition VALUES (%s)", (nom,))
           connexion.commit()
           cur.execute("INSERT INTO ExpositionPermanente VALUES (%s)", (nom,))
           connexion.commit()
           print("\nL'exposition permanente a été créé avec succès.")
       except psycopg2.Error as e:
           print("\nERREUR : Une erreur s'est produite lors de la création du exposition :", e)
   else:
       print("\nERREUR : unknown type d'exposition")
       return

def create_musee():
    print("---------creation d'un musee----------")
    nom = input("nom du musee: ")
    adresse = input("adresse: ")
    # Verification Donnees completes
    if not nom:
        print("\nERREUR : Veuillez fournir toutes les informations nécessaires pour créer le musee.")
        return
    # Verification le musee deja existe
    cur.execute("SELECT COUNT(*) FROM Musee WHERE nom = %s", (nom,))
    count = cur.fetchone()[0]
    if count > 0 :
        print("\nERREUR : Le musee existe déjà dans la base de données.")
        return
    # ajouter le musee
    try:
        cur.execute("INSERT INTO Musee VALUES (%s, %s)", (nom, adresse))
        connexion.commit()
        print("\nLe musee a été créé avec succès.")
    except psycopg2.Error as e:
        print("\nERREUR : Une erreur s'est produite lors de la création du exposition :", e)
        return

def create_guide():
    print("---------creation d'un guide----------")
    num = input("Num: ")
    nom = input("Nom: ")
    prenom = input("Prenom: ")
    adresse = input("Adresse: ")
    date_embauche = input("Date_embauche: ")
    exposition_temporaire = input("Exposition temporaire: ")
    # Verification Donnees completes
    if not num or not nom or not prenom or not adresse or not date_embauche:
        print("\nERREUR : Veuillez fournir toutes les informations nécessaires pour créer le guide.")
        return
    # TODO Verification format de date
    # Verification le guide deja existe
    cur.execute("SELECT COUNT(*) FROM Guide WHERE id = %s", (num,))
    count = cur.fetchone()[0]
    if count > 0 :
        print("\nERREUR : Le guide existe déjà dans la base de données.")
        return
    # ajouter le guide
    try:
        cur.execute("INSERT INTO Guide VALUES (%s, %s, %s, %s, %s::date, %s)", (num, nom, prenom, adresse, date_embauche, exposition_temporaire))
        connexion.commit()
        print("\nLe guide a été créé avec succès.")
    except psycopg2.Error as e:
        print("\nERREUR : Une erreur s'est produite lors de la création du guide :", e)
        return

def create_creneau():
    print("---------creation d'un creneau----------")
    guide = input("Guide: ")
    expo = input("Expo: ")
    jour = input("Nom du jour: ")
    heure_debut = input("Heure debut: ")
    heure_fin = input("Heure fin: ")
    # Verification Donnees completes
    if not guide or not expo or not jour or not heure_debut or not heure_fin:
        print("\nERREUR : Veuillez fournir toutes les informations nécessaires pour créer le creneau.")
        return
    # Verification le creneau deja existe
    cur.execute("SELECT COUNT(*) FROM Creneau WHERE guide = %s AND jour = %s AND heure_debut = %s", (guide,jour,heure_debut))
    count = cur.fetchone()[0]
    if count > 0 :
        print("\nERREUR : Le creneau existe déjà dans la base de données.")
        return
    # ajouter le guide
    try:
        cur.execute("INSERT INTO Creneau VALUES (%s, %s, %s, %s, %s)", (guide, expo, jour, heure_debut, heure_fin))
        connexion.commit()
        print("\nLe creneau a été créé avec succès.")
    except psycopg2.Error as e:
        print("\nERREUR : Une erreur s'est produite lors de la création du creneau :", e)
        return

def create_salle():
    print("---------creation d'une salle----------")
    num = input("Num: ")
    capacite = input("Capacité: ")
    exposition_temporaire = input("Nom du Exposition temporaire: ")
    # Verification Donnees completes
    if not num or not capacite or not exposition_temporaire:
        print("\nERREUR : Veuillez fournir toutes les informations nécessaires pour créer la salle.")
        return
    # Verification le creneau deja existe
    cur.execute("SELECT COUNT(*) FROM Salle WHERE num = %s", (num,))
    count = cur.fetchone()[0]
    if count > 0 :
        print("\nERREUR : La salle existe déjà dans la base de données.")
        return
    # ajouter la salless
    try:
        cur.execute("INSERT INTO Salle VALUES (%s, %s, %s)", (num, capacite, exposition_temporaire))
        connexion.commit()
        print("\nLa salle a été créé avec succès.")
    except psycopg2.Error as e:
        print("\nERREUR : Une erreur s'est produite lors de la création de la salle :", e)
        return


def create_panneau():
    print("---------creation d'un panneau----------")
    num = input("Num: ")
    texte = input("texte: ")
    exposition_temporaire = input("Nom du Exposition temporaire: ")
    salle = input("No. Salle: ")
    # Verification Donnees completes
    if not num or not texte or not exposition_temporaire or not salle:
        print("\nERREUR : Veuillez fournir toutes les informations nécessaires pour créer le panneau explicatif.")
        return
     # Verification le panneau deja existe
    cur.execute("SELECT COUNT(*) FROM PanneauExplicatif WHERE num = %s", (num,))
    count = cur.fetchone()[0]
    if count > 0 :
        print("\nERREUR : Le panneau existe déjà dans la base de données.")
        return
    # ajouter le panneau
    try:
        cur.execute("INSERT INTO PanneauExplicatif VALUES (%s, %s, %s, %s)", (num, texte, exposition_temporaire, salle))
        connexion.commit()
        print("\nLe panneau a été créé avec succès.")
    except psycopg2.Error as e:
        print("\nERREUR : Une erreur s'est produite lors de la création de panneau :", e)
        return

# ----------------------------Select-----------------------------
def cherche_oeuvre(nom) :
    sql = "SELECT * FROM Oeuvre WHERE titre='%s'" % nom
    cur.execute(sql)
    row = cur.fetchone()
    if (row) :
        return True
    else :
        return False

def affiche_oeuvre() :
    sql = "SELECT * FROM Oeuvre"
    cur.execute(sql)
    row = cur.fetchone()
    while(row):
        print(row[0])
        row = cur.fetchone()

def affiche_expo() :
        sql = "SELECT * FROM Exposition"
        cur.execute(sql)
        row = cur.fetchone()
        while(row):
            print(" - %s \n" % row[0])
            row = cur.fetchone()

def affiche_expo_temp() :
        sql = "SELECT * FROM ExpositionTemporaire"
        cur.execute(sql)
        row = cur.fetchone()
        while(row):
            print(" - %s \n" % row[0])
            row = cur.fetchone()

def affiche_expo_perm() :
        sql = "SELECT * FROM ExpositionPermanente"
        cur.execute(sql)
        row = cur.fetchone()
        while(row):
            print(" - %s \n" % row[0])
            row = cur.fetchone()

def affiche_auteur() :
    sql = "SELECT * FROM Auteur"
    cur.execute(sql)
    row = cur.fetchone()
    while(row):
        print(" - %s \n" % row[1])
        row = cur.fetchone()

def affiche_musee() :
    sql = "SELECT * FROM Musee"
    cur.execute(sql)
    row = cur.fetchone()
    while(row):
        print(" - %s \n" % row[0])
        row = cur.fetchone()

def affiche_emprunt() :
    sql = "SELECT * FROM Emprunte"
    cur.execute(sql)
    row = cur.fetchone()
    while(row):
        sql1 = "SELECT musee FROM Oeuvre WHERE titre = '%s'" % row[1]
        cur1.execute(sql1)
        row1 = cur1.fetchone()
        print(" - %s --> %s : %s\n" % (row1[0],row[0], row[1]))
        row = cur.fetchone()


def ajout_oeuvre() :
    titre=0
    while(cherche_oeuvre(titre) or titre == 0) :
        titre = input("Donner le titre de l'oeuvre : ")
    type = input("Donner le type de l'oeuvre (peinture,sculpture,photo) : ")
    date =" "
    while (validate(date) == False) :
        date = input("Entrer la date de création (exemple : 01-01-2023): ")
    format = input("Entrer les dimensions de l'oeuvre : ")
    verif=0
    while(verif==0) :
        print("indiquer le nom de l'auteur parmis ceux existants : \n")
        affiche_auteur()
        aut = input("Votre choix : ")
        sql = "SELECT * FROM Auteur WHERE nom='%s'" % aut
        cur.execute(sql)
        row = cur.fetchone()
        if (row) :
            verif = 1
            aut = row[0]
    verif=0
    while(verif==0) :
        print("indiquer le nom de l'exposition parmis celles existantes : \n")
        affiche_expo()
        expo = input("Votre choix : ")
        sql = "SELECT * FROM Exposition WHERE nom='%s'" % expo
        cur.execute(sql)
        row = cur.fetchone()
        if (row) :
            verif = 1
    verif=0
    while(verif==0) :
        print("indiquer le nom du musee parmis ceux existants : \n")
        affiche_musee()
        musee = input("Votre choix : ")
        sql = "SELECT * FROM Musee WHERE nom='%s'" % musee
        cur.execute(sql)
        row = cur.fetchone()
        if (row) :
            verif = 1
    sql = "INSERT INTO Oeuvre VALUES('%s','%s', TO_DATE('%s', 'DD-MM-YYYY'), '%s', NULL, 'TRUE', %d, '%s', '%s');" % (titre,type,date,format,aut,expo,musee)
    cur.execute(sql)

def affiche_Musee_dif() :
    sql = "SELECT * FROM Musee WHERE nom <> 'Le Louvre'"
    cur.execute(sql)
    row = cur.fetchone()
    while(row):
        print(" - %s \n" % row[0])
        row = cur.fetchone()

def affiche_oeuvre_musee(musee) :
    sql = "SELECT * FROM Oeuvre WHERE Oeuvre.musee = '%s'" % musee
    cur.execute(sql)
    row = cur.fetchone()
    while(row):
        print(" - %s \n" % row[0])
        row = cur.fetchone()


def emprunt() :
    verif=0
    while(verif==0) :
        print("indiquer le nom du musee à qui Le Louvre va emprunter parmis ceux existants : \n")
        affiche_Musee_dif()
        musee = input("Votre choix : ")
        sql = "SELECT * FROM Musee WHERE nom='%s'" % musee
        cur.execute(sql)
        row = cur.fetchone()
        if (row) :
            verif = 1
    verif=0
    while(verif==0) :
        print("indiquer le nom de l'oeuvre à emprunter parmis celles existantes : \n")
        affiche_oeuvre_musee(musee)
        oeuvre = input("Votre choix (0 pour sortir) : ")
        if (oeuvre=="0") :
            return
        sql = "SELECT * FROM Oeuvre WHERE musee='%s' AND titre='%s'" % (musee,oeuvre)
        cur.execute(sql)
        row = cur.fetchone()
        if (row) :
            sql1 = "SELECT * FROM Emprunte WHERE musee='Le Louvre' AND oeuvre='%s'" % oeuvre
            cur1.execute(sql1)
            row1 = cur1.fetchone()
            if (row1) :
                pass
            else :
                verif = 1
    debut =" "
    while (validate(debut) == False) :
        debut = input("Entrer la date de début d'emprunter (exemple : 01-01-2023): ")
    fin =" "
    while (validate(fin) == False) :
        fin = input("Entrer la date de fin d'emprunts (exemple : 01-01-2023): ")
    sql = "INSERT INTO Emprunte VALUES ('Le Louvre', '%s', TO_DATE('%s', 'DD-MM-YYYY'), TO_DATE('%s', 'DD-MM-YYYY'));" % (oeuvre,debut,fin)
    cur.execute(sql)

def pret() :
    verif=0
    while(verif==0) :
        print("indiquer le nom du musee à qui Le Louvre va prêter parmis ceux existants : \n")
        affiche_Musee_dif()
        musee = input("Votre choix : ")
        sql = "SELECT * FROM Musee WHERE nom='%s'" % musee
        cur.execute(sql)
        row = cur.fetchone()
        if (row) :
            verif = 1
    verif=0
    while(verif==0) :
        print("indiquer le nom de l'oeuvre à prêter parmis celles existantes : \n")
        affiche_oeuvre_musee('Le Louvre')
        oeuvre = input("Votre choix (0 pour sortir) : ")
        if (oeuvre=="0") :
            return
        sql = "SELECT * FROM Oeuvre WHERE musee='Le Louvre' AND titre='%s'" % oeuvre
        cur.execute(sql)
        row = cur.fetchone()
        if (row) :
            sql1 = "SELECT * FROM Emprunte WHERE musee='%s' AND oeuvre='%s'" % (musee,oeuvre)
            cur1.execute(sql1)
            row1 = cur1.fetchone()
            if (row1) :
                pass
            else :
                verif = 1
    debut =" "
    while (validate(debut) == False) :
        debut = input("Entrer la date de début d'emprunter (exemple : 01-01-2023): ")
    fin =" "
    while (validate(fin) == False) :
        fin = input("Entrer la date de fin d'emprunts (exemple : 01-01-2023): ")
    sql = "INSERT INTO Emprunte VALUES ('%s', '%s', TO_DATE('%s', 'DD-MM-YYYY'), TO_DATE('%s', 'DD-MM-YYYY'));" % (musee,oeuvre,debut,fin)
    cur.execute(sql)





def gestion_emprunts() :
    choix = 1
    while (choix!="0") :
        print("Que faire ? ")
        print("1 - Afficher les Emprunts/Prêts")
        print("2 - Emprunter une oeuvre")
        print("3 - Prêter une oeuvre")
        print("0 - Exit\n")
        choix = input("Entrer votre choix :")
        if (choix=="1") :
            affiche_emprunt()
        elif (choix=="2") :
            emprunt()
        elif (choix=="3") :
            pret()



# Définition des fonctions d'affichage des oeuvres et des expositions

def afficherOeuvres():
    sql = "SELECT titre, date, Auteur.nom FROM Oeuvre JOIN Auteur ON Auteur.id = Oeuvre.auteur"
    cur.execute(sql)
    row = cur.fetchone()
    while(row):
        print(" - ",row[0],"|",row[1],"|",row[2])
        row=cur.fetchone()
    print()


def afficherOeuvresAuteur():
    affiche_auteur()
    auteur_str = input("Qui est l'auteur ? ")
    print("Voici les différentes oeuvres de %s :\n" % auteur_str)
    sql = "SELECT o.titre, o.date, a.nom FROM Oeuvre o JOIN Auteur a ON a.id = o.auteur WHERE a.nom = '%s' " % auteur_str
    cur.execute(sql)
    row = cur.fetchone()
    while(row) :
        print(row[0],row[1])
        row = cur.fetchone()
    print()

def afficherExpositions():
    sql = "SELECT nom FROM ExpositionTemporaire"
    cur.execute(sql)
    row = cur.fetchone()
    print("Les différentes expositions temporaires : \n")
    while(row) :
        print(row[0])
        row = cur.fetchone()
    print()
    sql = "SELECT nom FROM ExpositionPermanente"
    cur.execute(sql)
    row = cur.fetchone()
    print("Les différentes expositions permanente : \n")
    while(row) :
        print(row[0])
        row = cur.fetchone()
    print()

def infoExpoPermanente() :
    affiche_expo_perm()
    expo = input("L'exposition ? ")
    print("Les différentes oeuvres : \n")
    sql = "SELECT o.titre, o.date, a.nom FROM Oeuvre o JOIN Auteur a ON a.id = o.auteur WHERE o.exposition = '%s'" % expo
    cur.execute(sql)
    row = cur.fetchone()
    while (row) :
        print(row[0]," fait en ",row[1]," par ",row[2])
        row = cur.fetchone()
    print()
    sql = "SELECT c.heure_debut, c.heure_fin, g.nom, g.prenom FROM Creneau c JOIN Exposition e ON e.nom = c.expo INNER JOIN Guide g ON g.id = c.guide	WHERE e.nom = '%s';" % expo
    cur.execute(sql)
    row = cur.fetchone()
    while(row) :
        print("De ",row[0]," à ",row[1]," avec ",row[2],row[3]) % (row[0],row[1],row[2],row[3])
        row=cur.fetchone()
    print()

def infoExpoTemporaire() :
    affiche_expo_temp()
    expo = input("L'exposition ? ")
    print("Les différentes oeuvres : \n")
    sql = "SELECT o.titre, o.date, a.nom FROM Oeuvre o JOIN Auteur a ON a.id = o.auteur WHERE o.exposition = '%s'" % expo
    cur.execute(sql)
    row = cur.fetchone()
    while (row) :
        print(row[0]," fait en ",row[1]," par ",row[2])
        row = cur.fetchone()
    print()
    sql = "SELECT s.num, s.capacite	FROM Salle s JOIN Exposition e ON e.nom = s.exposition_temporaire	WHERE e.nom = '%s';" % expo
    cur.execute(sql)
    row = cur.fetchone()
    while(row) :
        print("La salle ",row[0]," avec une capacité de ",row[1]," personnes")
        row = cur.fetchone()
    print()


def quitter():
	exit()
# Définition du dictionnaire des actions
actions = {
	'1': afficherOeuvres,
	'2': afficherExpositions,
	'3': afficherOeuvresAuteur,
	'4' : infoExpoPermanente,
	'5' : infoExpoTemporaire,
	'q': quitter
}

# Définition de la fonction de switch-case
def switch_case(value) :
    print("-------------------------------------------------------")
    func = actions.get(value)
    if(func) :
        func()
    else :
        print("Aucune action trouvée pour cette valeur")
    print("-------------------------------------------------------")



choix = 1
while(check_bdd()) :
    print("\nConnexion\n")
    id = input("Identifiant :")
    mdp = input("Mot de pass :")
    sql = "SELECT * FROM Comptes WHERE id='%s'AND mdp='%s'" % (id,mdp)
    print(choix)
    cur.execute(sql)
    row = cur.fetchone()
    if (row) :
        if (row[2]=='admin') :
            while (choix!="0") :
                print()
                print("MENU\n")
                print("1 - Ajouter un auteur")
                print("2 - Ajouter une exposition")
                print("3 - Ajouter une oeuvre")
                print("4 - Gerer les emprunts")
                print("0 - Exit\n")
                choix = input("Entrer votre choix :")
                if (choix=="1") :
                    create_auteur()
                elif (choix=="2") :
                    create_exposition()
                elif (choix=="3") :
                    ajout_oeuvre()
                elif (choix=="4") :
                    gestion_emprunts()
                elif (choix=="0") :
                    quitter()
        if(row[2]=='utilisateur') :
            print("Bienvenue sur le site du Louvre.")
            while(True):
                print("Que souhaitez vous faire ?")
                print("1. M'informer sur les différentes oeuvres")
                print("2. M'informer sur les différentes expositions")
                print("3. M'informer sur les différentes oeuvres de tel auteur")
                print("4. M'informer sur une exposition permanente en particulier")
                print("5. M'informer sur une exposition temporaire en particulier")
                print("q : Quitter")
                choix = input("Votre choix : ")
                switch_case(choix)



connexion.commit()
connexion.close()
